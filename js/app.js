const token = 'bearer fdbe460e4c5e9a51623f3cda4f7436bd9439e2da';
const spaceId = '60fe4c2bb4d37af76aba57ec';

const client = new window.AvayaClientServices();
let user, activeSpeaker;

function bindEvents(){
    document.getElementById('joinSpace').addEventListener("click", joinSpace);
    document.getElementById('startMeeting').addEventListener("click", startMeeting);
}

bindEvents();

const isBase64 = str => {
    return Base64.isBase64(str);
};
const base64Decode = str => {
    return Base64.decode(str);
};

function joinSpace(){
    axios({
        method: 'GET',
        url: `https://loganstagingapis.esna.com/api/spaces/${spaceId}/join`,
        headers:
        {
            'Authorization': token,
        },
    }).then((res) => {
        console.log(res)
        mpassToken()
    })
}

function mpassToken(){
    axios({
        method: 'GET',
        url: `https://loganstaging.esna.com/api/mediasessions/mpaas/token/${spaceId}`,
        data: {
            "displayname": "Andrew API",
            "username": "AndrewAPI"
        },
        headers:
        {
            'Authorization': token,
        },
    }).then(mpaasInfo => {
        console.log(mpaasInfo)

        const mpaasToken = mpaasInfo.data.token;

        const userConfiguration = {
            mediaServiceContext: mpaasToken, // <- Set the MPaaS token here as the service context
            callUserConfiguration:
            {
                videoEnabled: true // A boolean value indicating whether application allows video or not.
            },
            wcsConfiguration: {
                enabled: true // Boolean value indicating whether the WCS provider is enabled.
            },
            collaborationConfiguration: {
                imageQuality: 10, // Default image quality used in content sharing. Web Collaboration Server may override this value. Value ranges from 1 to 10, where 10 is highest image quality.
                contentSharingWorkerPath: `../libs/AvayaClientServicesWorker.js`,
            },
        };
        
        user = client.createUser(userConfiguration);
        console.log("user", user)
        initiateSpacesCall();
    })
}

function startRemoteVideo(stream) {
    console.log('%c startRemoteVideo() ', 'background: #0000ff; color: #fff');
    let video = document.querySelector("#remoteVideoElement");
    video.srcObject = stream;
}

function stopRemoteVideo() {
    console.log('%c stopRemoteVideo() ', 'background: #0000ff; color: #fff');
    let video = document.querySelector("#remoteVideoElement");
    video.srcObject = null;
}

function onNetworkEventsCallback(events){
    if (!events || !events.length) {
        console.warn('event ignored - invalid event parameters', events);
        return;
    }
    events.forEach(event => {
        processNetworkEvent(event);
    });
};

function processNetworkEvent(event) {
    if (isActiveSpeakerEvent(event)) {
        processActiveSpeakerEvent(event);
    }
}

function isActiveSpeakerEvent(event) {
    const res =
        event &&
        event.resourceId &&
        event.resourceType === "CONFERENCE" &&
        event.details &&
        event.details.sessions &&
        event.details.sessions.length > 0 &&
        event.trigger === "SUBSCRIPTION" &&
        'ACTIVE_SPEAKER_NOTIFY' === event.name;
    return res;
}

function processActiveSpeakerEvent(event) {
    let sender;
    let mdsrvSessionId;
    let otherSpeakers = [];
    let topicId;
    event.details.sessions.forEach(speaker => {
        if (speaker.opaqueData && isBase64(speaker.opaqueData)) {
            try {
                const decodedData = base64Decode(speaker.opaqueData);
                if (decodedData && decodedData.length > 0) {
                    const opaqueData = JSON.parse(decodedData);
                    // console.log("speaker information" + opaqueData)
                    const attendee = opaqueData.user;
                    topicId = opaqueData.topicId || topicId;
                    if (!sender && attendee) {
                        mdsrvSessionId = speaker.id;
                        sender = attendee;
                    } else if (attendee) {
                        otherSpeakers.push({
                            mdsrvSessionId: speaker.id,
                            attendee
                        });
                    }
                }
            } catch (error) {
                console.log("failed to decode or parse speaker information" + error)

            }
        } else {
            console.log('Invalid speaker information', event);
        }
    });

    if (topicId) {
        emitOnActiveSpeakerChanged({
            _id: getGUID(),
            topicId,
            sender,
            content: {
                mediaSession: { mdsrvSessionId },
                otherSpeakers
            }
        });
    }
}

function emitOnActiveSpeakerChanged(event) {
    // console.log('%c Active speaker changed: ', 'background: #ff6a00; color: #fff');
    activeSpeaker = event.sender._id
    // console.log(this.activeSpeaker)
    // this.EventEmitter.dispatch('state', { data: { activeSpeaker: this.activeSpeaker } })
}

function initiateSpacesCall(){
    const calls = user.getCalls()
    const call = calls.createDefaultCall();

    call.setVideoMode(window.AvayaClientServices.Services.Call.VideoMode.SEND_RECEIVE);
    call.setWebCollaboration(true); // Set to true if you want to enable web collaboration

    call.addOnCallIncomingVideoAddRequestDeniedCallback((call) => {
        console.log("IncomingVideoAddRequestDenied");
    });
    call.addOnCallFailedCallback((call, callException) => {
        console.log("call failed", callException);
    });
    call.addOnCallEndedCallback((call, event) => {
        console.log("call ended");
        console.log(event);
    });
    call.addOnCallEstablishingCallback((call) => {
        console.log("call establishing...");
    });

    let acceptedEvents = {
        category: ["CONFERENCE", "SESSION"],
        events: ['ACTIVE_SPEAKER_NOTIFY', 'RESOURCE_NETWORK_STATUS'],
        excludedEvents: []
    }
    
    calls.setOnNetworkEvents(acceptedEvents, onNetworkEventsCallback).then((res) => {
        console.log('%c onNetworkEventsCallback() ', 'background: #ff6a00; color: #fff');
    })

    call.addOnCallVideoChannelsUpdatedCallback((call) => {
        console.log(1, client);
        let mediaEngine = client.getMediaServices();
        console.log(2);
        let videoChannels = call.getVideoChannels();

        console.log(videoChannels)

        const remoteStream = mediaEngine.getVideoInterface().getRemoteMediaStream(videoChannels[0].getChannelId());
        if (window.AvayaClientServices.Base.Utils.isDefined(remoteStream)) {
            startRemoteVideo(remoteStream);
        }
    })
}