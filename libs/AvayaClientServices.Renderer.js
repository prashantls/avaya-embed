/*eslint-disable no-undef, no-restricted-globals*/
(function(jQueryHandle, undefined) {
  var $ = jQueryHandle;

  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */
  /**
   * Description
   *
   */
  (function(AvayaClientServices) {
    'use strict';
    /**
     * @namespace AvayaClientServices
     */
    (function(AvayaClientServices) {
      AvayaClientServices = AvayaClientServices || {};
    })(AvayaClientServices);
    /**
     * @namespace AvayaClientServices.Renderer
     */
    (function(AvayaClientServices) {
      AvayaClientServices.Renderer = AvayaClientServices.Renderer || {};
    })(AvayaClientServices);
    /**
     * AvayaClientServices.Renderer.Konva namespace contains all classes that belong to Konva renderer.
     * @namespace AvayaClientServices.Renderer.Konva
     */
    (function(AvayaClientServices) {
      AvayaClientServices.Renderer.Konva =
        AvayaClientServices.Renderer.Konva || {};
    })(AvayaClientServices);
  })(AvayaClientServices);
  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */
  (function(AvayaClientServices) {
    'use strict';
    /**
     * ContentSharingRenderer class is an interface for content sharing renderers.
     * @class
     * @abstract
     * @define AvayaClientServices.Renderer.ContentSharingRenderer
     * @memberOf AvayaClientServices.Renderer
     */
    function ContentSharingRenderer() {
      /**
       *
       * @type {AvayaClientServices.Services.Collaboration.ContentSharing}
       * @protected
       */
      this._contentSharing = undefined;
    }

    ContentSharingRenderer.prototype =
      /** @lends AvayaClientServices.Renderer.ContentSharingRenderer.prototype **/
      {
        /**
         * @description
         * Initializes the content sharing renderer.
         *
         * @public
         * @function
         * @param {AvayaClientServices.Services.Collaboration.ContentSharing} contentSharing Content sharing instance.
         * @param {string} containerId ID of DOM element where content sharing will be append to.
         * @returns {void}
         */
        init: function(contentSharing, containerId) {
          this._contentSharing = contentSharing;

          contentSharing.addOnCursorReceivedCallback(
            function(contentSharing, point) {
              this.showCursor(point);
            }.bind(this)
          );

          contentSharing.addOnContentSharingStartedCallback(
            function(contentSharing, startingParticipant) {
              var voipConfiguration = {
                konvaSharingFramesAvarageLimit:
                  AvayaClientServices.Base.Utils.queryJSONObject(
                    contentSharing,
                    '_collaboration._config.mediaConfiguration.voipConfigurationVideo.konvaSharingFramesAvarageLimit'
                  ) || 2.5,
                konvaSharingFramesWarningLimit:
                  AvayaClientServices.Base.Utils.queryJSONObject(
                    contentSharing,
                    '_collaboration._config.mediaConfiguration.voipConfigurationVideo.konvaSharingFramesWarningLimit'
                  ) || 1,
                konvaSharingGracePeriod:
                  AvayaClientServices.Base.Utils.queryJSONObject(
                    contentSharing,
                    '_collaboration._config.mediaConfiguration.voipConfigurationVideo.konvaSharingGracePeriod'
                  ) || 15,
                konvaSharingMeasurementsInterval:
                  AvayaClientServices.Base.Utils.queryJSONObject(
                    contentSharing,
                    '_collaboration._config.mediaConfiguration.voipConfigurationVideo.konvaSharingMeasurementsInterval'
                  ) || 0.5,
                konvaSharingMeasurementsWindowDuration:
                  AvayaClientServices.Base.Utils.queryJSONObject(
                    contentSharing,
                    '_collaboration._config.mediaConfiguration.voipConfigurationVideo.konvaSharingMeasurementsWindowDuration'
                  ) || 10,
                konvaSharingDebugMode:
                  AvayaClientServices.Base.Utils.queryJSONObject(
                    contentSharing,
                    '_collaboration._config.mediaConfiguration.voipConfigurationVideo.konvaSharingDebugMode'
                  ) || false
              };
              this._setConfig(voipConfiguration);
              this.start(containerId);
            }.bind(this)
          );

          contentSharing.addOnContentSharingEndedCallback(
            function(contentSharing, endingParticipant) {
              this.stop();
            }.bind(this)
          );

          contentSharing.addOnContentSharingPausedCallback(
            function(contentSharing, pausingParticipant) {
              this.pause();
            }.bind(this)
          );

          contentSharing.addOnContentSharingResumedCallback(
            function(contentSharing, resumingParticipant) {
              this.unpause();
            }.bind(this)
          );

          contentSharing.addOnFrameReceivedCallback(
            function(contentSharing, frame) {
              this.drawFrame(frame);
            }.bind(this)
          );

          contentSharing.addOnFrameChangedCallback(
            function(contentSharing, frame) {
              this.drawFrame(frame);
            }.bind(this)
          );
        },
        /**
         * @description
         * Indicates the cursor position on a screen as a red dot.
         * To be able to invoke this function, the user who shares his screen has to send information about cursor coordinates.
         *
         * @public
         * @abstract
         * @function
         * @param {AvayaClientServices.Services.Collaboration.Point} point Current position of the cursor.
         * @returns {void}
         */
        showCursor: function(point) {},
        /**
         * @description
         * Starts screen sharing.
         * Function creates DOM element with the given ID where the screen sharing will be displayed in.
         *
         * @abstract
         * @public
         * @function
         * @returns {void}
         */
        start: function() {},
        /**
         * @description
         * Stops screen sharing.
         * Function removes the DOM element where the screen sharing was displayed in.
         *
         * @abstract
         * @public
         * @function
         * @returns {void}
         */
        stop: function() {},
        /**
         * @description
         * Pauses screen sharing.
         * Screen sharing can be paused only by the user who started the screen sharing.
         *
         * @abstract
         * @public
         * @function
         * @returns {void}
         */
        pause: function() {},
        /**
         * @description
         * Unpauses the screen sharing and removes the pause layer from the canvas.
         *
         * @abstract
         * @public
         * @function
         * @returns {void}
         *
         */
        unpause: function() {},
        /**
         * @description
         * Draws on a screen frames received from the server.
         *
         * @abstract
         * @public
         * @function
         * @param {AvayaClientServices.Services.Collaboration.Frame} frame Frame that is going to be drawn.
         * @returns {void}
         */
        drawFrame: function(frame) {},

        /**
         * @type {function}
         * @public
         * @function
         * @param {String} reason
         */
        onOverloaded: function(reason) {},

        /**
         * @description
         * Performance monitor, monitor the load of the frames processing, and reduce the image quality if needed.
         * @public
         * @function
         * @returns {void}
         */
        performanceMonitorStart: function() {},

        /**
         * @description
         * stops the performance monitor, and clear the interval
         * @public
         * @function
         * @returns {void}
         */
        performanceMonitorStop: function() {}
      };

    AvayaClientServices.Renderer.ContentSharingRenderer = ContentSharingRenderer;
  })(AvayaClientServices);
  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */

  (function(AvayaClientServices) {
    'use strict';

    /**
     * KonvaContentSharingRenderer extends Collaboration module ContentSharingRenderer.
     * It allows to render the images received from the server.
     *
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer
     * @memberOf AvayaClientServices.Renderer.Konva
     * @extends AvayaClientServices.Renderer.ContentSharingRenderer
     * @constructor
     */
    function KonvaContentSharingRenderer() {
      /**
       * Visibility event name for different browsers
       * @type {string}
       * @private
       */
      this._tabVisibilityEventName = undefined;

      /**
       * Visibility state name for different browsers
       * @type {string}
       * @private
       */
      this._tabVisibilityStateName = undefined;

      this._tabVisibilityEventName = AvayaClientServices.Base.Utils.getBrowserTabVisibilityParams()[0];
      this._tabVisibilityStateName = AvayaClientServices.Base.Utils.getBrowserTabVisibilityParams()[1];

      /**
       *
       * @type {string}
       * @private
       */
      this._state =
        AvayaClientServices.Renderer.Konva.KonvaContentSharingState.STOPPED;
      /**
       *
       * @type {number}
       * @private
       */
      this._width = 0;
      /**
       *
       * @type {number}
       * @private
       */
      this._height = 0;
      /**
       *
       * @type {Konva.Circle}
       * @private
       */
      this._cursor = undefined;
      /**
       * default pixelRatio has been added to make sure that
       * every device is not calculating different
       * pixelRatio which would lead to zoom duplicating
       * or wrong display during screen sharing
       * @type {number}
       * @private
       */
      Konva.pixelRatio = 1;
      /**
       *
       * @type {Konva.FastLayer}
       * @private
       */
      this._zoomLayer = new Konva.FastLayer();
      /**
       *
       * @type {Konva.FastLayer}
       * @private
       */
      this._cursorLayer = new Konva.FastLayer();
      /**
       *
       * @type {Konva.FastLayer}
       * @private
       */
      this._pauseLayer = new Konva.FastLayer();
      /**
       *
       * @type {Konva.Stage}
       * @private
       */
      this._zoomStage = undefined;
      /**
       *
       * @type {Element}
       * @private
       */
      this._screenSharingBg = undefined;
      /**
       *
       * @type {number}
       * @private
       */
      this._scale = 1;

      /**
       * An id generated internally to this class to associate frames with a particular screen sharing session.
       *
       * @type {number}
       * @private
       */
      this._screenSharingSession = 0;

      /**
       * A base html canvas to draw the screen sharing blocks/frames to.
       *
       * @type {HTMLCanvasElement}
       * @private
       */
      this._htmlCanvas = undefined;

      /**
       * The current frame being processed by the renderer (if any)
       *
       * @type {AvayaClientServices.Services.Collaboration.Frame}
       * @private
       */
      this._currentFrame = undefined;

      /**
       * A queue of frames to be rendered once the current frame is complete
       *
       * @type {AvayaClientServices.Services.Collaboration.Frame[]}
       * @private
       */
      this._queuedFrames = [];

      /**
       * A performance monitor timer
       */
      this._performanceMonitor = undefined;
    }

    KonvaContentSharingRenderer.prototype = Object.create(
      AvayaClientServices.Renderer.ContentSharingRenderer.prototype
    );

    /**
     * @description
     * Browser tab visibility state change handle
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_handleVisibilityChange
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype._handleVisibilityChange = function() {
      if (
        AvayaClientServices.Base.Utils.isApplicationTabActive(
          this._tabVisibilityStateName
        ) &&
        !this._performanceMonitor &&
        !this._isContentSharingStopped()
      ) {
        this.performanceMonitorStart();
      } else {
        this.performanceMonitorStop();
      }
    };

    /**
     * @description
     * Returns width of the stage
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#getWidth
     * @returns {Number}
     */
    KonvaContentSharingRenderer.prototype.getWidth = function() {
      return this._width;
    };

    /**
     * @description
     * Returns height of the stage
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#getHeight
     * @returns {Number}
     */
    KonvaContentSharingRenderer.prototype.getHeight = function() {
      return this._height;
    };

    /**
     * @description
     * Draws on a screen frames received from the server.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#drawFrame
     * @param {AvayaClientServices.Services.Collaboration.Frame} frame Frame that is going to be drawn.
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.drawFrame = function(frame) {
      if (this._isContentSharingStopped()) {
        return;
      }

      try {
        if (!!this._currentFrame) {
          if (frame.isKeyFrame()) {
            AvayaClientServices.Base.Logger.debug(
              AvayaClientServices.Renderer.LoggerTags
                .KONVA_CONTENT_SHARING_RENDERER,
              'Queuing a key frame for rendering and wiping the queue of ' +
                this._queuedFrames.length +
                ' frames. '
            );
            this._queuedFrames = [frame];
          } else {
            this._queuedFrames.push(frame);
            AvayaClientServices.Base.Logger.debug(
              AvayaClientServices.Renderer.LoggerTags
                .KONVA_CONTENT_SHARING_RENDERER,
              'Queued an intra frame for rendering.  Queue size: ' +
                this._queuedFrames.length
            );
          }
          return;
        }
        this._currentFrame = frame;

        AvayaClientServices.Base.Logger.debug(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'Drawing a ' + (frame.isKeyFrame() ? 'key' : 'intra') + ' frame. '
        );
        if (this._width !== frame.getWidth() && frame.getWidth() > 0) {
          this._zoomStage.setWidth(frame.getWidth() * this._scale);
          this._width = frame.getWidth();
          this._htmlCanvas.width = this._width;

          if (
            this._state ===
            AvayaClientServices.Renderer.Konva.KonvaContentSharingState.PAUSED
          ) {
            this._redrawPauseLayer();
          }
        }

        if (this._height !== frame.getHeight() && frame.getHeight() > 0) {
          this._zoomStage.setHeight(frame.getHeight() * this._scale);
          this._height = frame.getHeight();
          this._htmlCanvas.height = this._height;

          if (
            this._state ===
            AvayaClientServices.Renderer.Konva.KonvaContentSharingState.PAUSED
          ) {
            this._redrawPauseLayer();
          }
        }

        var screenSharingSessionForFrame = this._screenSharingSession;
        var dfds = frame.getBitmaps().map(this._generateImage.bind(this));

        $.when.apply(this, dfds).then(
          function() {
            var bitmapImageList = Object.values(arguments);
            bitmapImageList.forEach(
              function(item) {
                if (AvayaClientServices.Base.Utils.isDefined(item)) {
                  try {
                    if (
                      !!this._canvasContext &&
                      screenSharingSessionForFrame ===
                        this._screenSharingSession
                    ) {
                      this._canvasContext.drawImage(
                        item.image,
                        item.bitmap.getXOffset(),
                        item.bitmap.getYOffset()
                      );
                    } else {
                      AvayaClientServices.Base.Logger.debug(
                        AvayaClientServices.Renderer.LoggerTags
                          .KONVA_WHITEBOARD_RENDERER,
                        'Skip draw image because sharing has stopped or changed'
                      );
                    }
                  } catch (e) {
                    AvayaClientServices.Base.Logger.error(
                      AvayaClientServices.Renderer.LoggerTags
                        .KONVA_WHITEBOARD_RENDERER,
                      'Error while processing loaded image. ',
                      e
                    );
                  } finally {
                    window.URL.revokeObjectURL(item.image.src);
                  }
                }
              }.bind(this)
            );

            this._drawReceivedFrame(screenSharingSessionForFrame);
            this._currentFrame = undefined;
            if (this._queuedFrames.length > 0) {
              var nextFrame = this._queuedFrames.shift();
              AvayaClientServices.Base.Logger.debug(
                AvayaClientServices.Renderer.LoggerTags
                  .KONVA_CONTENT_SHARING_RENDERER,
                'De-queuing a ' +
                  (nextFrame.isKeyFrame() ? 'key' : 'intra') +
                  ' frame for rendering. '
              );
              this.drawFrame(nextFrame);
            }
          }.bind(this)
        );
      } catch (error) {
        AvayaClientServices.Base.Logger.debug(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'Error when drawing a frame: ' + error
        );
        this._currentFrame = undefined;
      }
    };

    /**
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_drawReceivedFrame
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#
     * @param screenSharingSessionForFrame - screenSharingSession id for this frame
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype._drawReceivedFrame = function(
      screenSharingSessionForFrame
    ) {
      if (this._isContentSharingStopped()) {
        return;
      } else if (this._screenSharingSession !== screenSharingSessionForFrame) {
        return;
      }

      var image = this._getImageFromCanvas(this._htmlCanvas);
      this._zoomLayer.removeChildren();
      this._zoomLayer.add(image);
      this._zoomLayer.draw();
    };

    /**
     * Function returns Konva.Image from canvas.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_getImageFromLayer
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#
     * @param {HTMLCanvasElement} canvas
     * @returns {Konva.Image}
     */
    KonvaContentSharingRenderer.prototype._getImageFromCanvas = function(
      canvas
    ) {
      return new Konva.Image({
        x: 0,
        y: 0,
        image: canvas
      });
    };

    /**
     * Checks if sharing is stopped
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_isContentSharingStopped
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#
     * @returns {AvayaClientServices.Renderer.Konva.KonvaContentSharingState}
     */
    KonvaContentSharingRenderer.prototype._isContentSharingStopped = function() {
      return (
        this._state ===
        AvayaClientServices.Renderer.Konva.KonvaContentSharingState.STOPPED
      );
    };

    /**
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_generateImage
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#
     * @param {AvayaClientServices.Services.Collaboration.Bitmap} bitmap
     * @returns {AvayaClientServices.Base.Promise}
     */
    KonvaContentSharingRenderer.prototype._generateImage = function(bitmap) {
      var dfd = $.Deferred();
      var image = new Image();

      var screenSharingSessionForFrame = this._screenSharingSession;
      image.onload = function onImageLoaded() {
        if (this._config && this._config.konvaSharingDebugMode) {
          var timeoutPromise = function() {
            var def = $.Deferred();
            setTimeout(function() {
              def.resolve();
            }, 2000);
            return def.promise();
          };
          timeoutPromise().then(
            function() {
              if (
                screenSharingSessionForFrame === this._screenSharingSession &&
                this._canvasContext
              ) {
                dfd.resolve({ bitmap: bitmap, image: image });
              } else {
                window.URL.revokeObjectURL(image.src);
                AvayaClientServices.Base.Logger.debug(
                  AvayaClientServices.Renderer.LoggerTags
                    .KONVA_WHITEBOARD_RENDERER,
                  'Ignoring an image that loaded after its sharing session ended.'
                );
                dfd.resolve();
              }
            }.bind(this)
          );
        } else {
          if (
            screenSharingSessionForFrame === this._screenSharingSession &&
            this._canvasContext
          ) {
            dfd.resolve({ bitmap: bitmap, image: image });
          } else {
            window.URL.revokeObjectURL(image.src);
            AvayaClientServices.Base.Logger.debug(
              AvayaClientServices.Renderer.LoggerTags.KONVA_WHITEBOARD_RENDERER,
              'Ignoring an image that loaded after its sharing session ended.'
            );
            dfd.resolve();
          }
        }
      }.bind(this);
      image.onerror = function onImageError() {
        AvayaClientServices.Base.Logger.warn(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'An image failed to load.'
        );
        dfd.resolve();
        window.URL.revokeObjectURL(image.src);
      };

      image.src = bitmap.getBlobData();

      return dfd.promise();
    };

    /**
     * @description
     * Set voipConfigurationVideo config
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_setConfig
     * @param {AvayaClientServices.Config.VoIPConfigurationVideo} config
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype._setConfig = function(config) {
      this._config = config;
    };

    /**
     * @description
     * Indicates the cursor position on a screen as a red dot.
     * To be able to invoke this function, the user who shares his screen has to send information about cursor coordinates.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#showCursor
     * @param {AvayaClientServices.Services.Collaboration.Point} point Current position of the cursor.
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.showCursor = function(point) {
      if (this._isContentSharingStopped()) {
        return;
      }

      var tween = new Konva.Tween({
        node: this._cursor,
        duration: 0.1,
        easing: Konva.Easings.EaseIn,
        x: point.getX(),
        y: point.getY(),
        onFinish: function() {
          tween.destroy();
        }
      });
      tween.play();
    };

    /**
     * @description
     * Stops screen sharing.
     * Function removes the DOM element where the screen sharing was displayed in.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#stop
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.stop = function() {
      AvayaClientServices.Base.Logger.info(
        AvayaClientServices.Renderer.LoggerTags.KONVA_CONTENT_SHARING_RENDERER,
        'Screen sharing stopped. Sharing SessionId: ' +
          this._screenSharingSession
      );

      this._queuedFrames = [];
      this._htmlCanvas = undefined;
      this._canvasContext = undefined;
      this._zoomLayer.destroyChildren();
      this._cursorLayer.destroyChildren();
      this._pauseLayer.destroyChildren();
      this._width = 0;
      this._height = 0;
      if (this._zoomStage) {
        this._zoomStage.destroyChildren();
        this._zoomStage.setWidth(0);
        this._zoomStage.setHeight(0);
        this._zoomStage.draw();
        this._zoomStage.destroy();
        this._zoomStage = undefined;
      }

      $(this._screenSharingBg).remove();
      this._screenSharingBg = undefined;

      document.removeEventListener(
        this._tabVisibilityEventName,
        this._handleVisibilityChange.bind(this),
        false
      );
      this.performanceMonitorStop();
      this._config = undefined;
      this._state =
        AvayaClientServices.Renderer.Konva.KonvaContentSharingState.STOPPED;
    };

    /**
     * @description
     * Starts screen sharing.
     * Function creates DOM element with the given ID where the screen sharing will be displayed in.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#start
     * @param {string} containerId ID of DOM element where the content sharing will be append to.
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.start = function(containerId) {
      if (this._isContentSharingStopped()) {
        AvayaClientServices.Base.Logger.info(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'Screen sharing started.'
        );
        var screenSharingBackground = document.createElement('div');
        screenSharingBackground.setAttribute('id', containerId + '-bg');
        screenSharingBackground.classList.add('hide');

        this._screenSharingBg = screenSharingBackground;
        $('#' + containerId)
          .parent()
          .append(screenSharingBackground);

        this._zoomStage = new Konva.Stage({
          container: containerId,
          width: this._width,
          height: this._height
        });

        this._zoomStage.scale({
          x: this._scale,
          y: this._scale
        });

        this._zoomStage.add(this._zoomLayer);
        this._zoomStage.add(this._cursorLayer);
        this._zoomStage.add(this._pauseLayer);

        this._htmlCanvas = document.createElement('canvas');
        this._htmlCanvas.width = this._width;
        this._htmlCanvas.height = this._height;
        this._canvasContext = this._htmlCanvas.getContext('2d');

        this._cursor = new Konva.Circle({
          x: -10,
          y: -10,
          radius: 5,
          fill: '#EF5353'
        });
        this._cursorLayer.add(this._cursor);
        this._state =
          AvayaClientServices.Renderer.Konva.KonvaContentSharingState.STARTED;
        this._zoomLayer.destroyChildren();
        this._zoomStage.draw();
      } else {
        AvayaClientServices.Base.Logger.info(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'Screen sharing re-started. ',
          this._state
        );
      }

      setTimeout(
        function() {
          if (!this._isContentSharingStopped()) {
            this.performanceMonitorStart();
          }
        }.bind(this),
        this._config.konvaSharingGracePeriod * 1000
      );

      if (
        typeof document.addEventListener === 'undefined' ||
        this._tabVisibilityStateName === undefined
      ) {
        AvayaClientServices.Base.Logger.info(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'Page Visibility API is not supported by this browser.'
        );
      } else {
        document.addEventListener(
          this._tabVisibilityEventName,
          this._handleVisibilityChange.bind(this),
          false
        );
      }

      // Increment the session id and loop
      this._screenSharingSession =
        this._screenSharingSession < 10000 ? this._screenSharingSession + 1 : 0;
      AvayaClientServices.Base.Logger.info(
        AvayaClientServices.Renderer.LoggerTags.KONVA_CONTENT_SHARING_RENDERER,
        'Sharing SessionId: ' + this._screenSharingSession
      );
    };

    /**
     * @description
     * Pauses screen sharing.
     * Screen sharing can be paused only by the user who started the screen sharing.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#pause
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.pause = function() {
      AvayaClientServices.Base.Logger.info(
        AvayaClientServices.Renderer.LoggerTags.KONVA_CONTENT_SHARING_RENDERER,
        'Screen sharing paused.'
      );
      this._state =
        AvayaClientServices.Renderer.Konva.KonvaContentSharingState.PAUSED;
      if (this._width > 0) {
        this._redrawPauseLayer();
      } else if (
        !this._contentSharing.isSharingApplicationWindow() &&
        !this._contentSharing.isSharingFullScreen() &&
        this._contentSharing.isSharingApplicationWindow() !== undefined
      ) {
        this._width = 800;
        this._height = 600;
        this._zoomStage.setWidth(this._width * this._scale);
        this._zoomStage.setHeight(this._height * this._scale);
        this._redrawPauseLayer();
      }
    };

    /**
     * @description
     * Function draws pause layer on a screen.
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_redrawPauseLayer
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#
     * @return {void}
     */
    KonvaContentSharingRenderer.prototype._redrawPauseLayer = function() {
      this._pauseLayer.removeChildren();
      var background = new Konva.Rect({
        x: 0,
        y: 0,
        opacity: 0.1,
        width: this._width,
        height: this._height,
        fill: '#00897b'
      });
      this._pauseLayer.add(background);

      var pauseRectangleWidth = 20;
      var pauseRectangleHeight = 80;
      var offsetX = 10;

      var leftPauseRectangle = new Konva.Rect({
        x: this._width / 2 - pauseRectangleWidth - offsetX,
        y: (this._height - pauseRectangleHeight) / 2,
        width: pauseRectangleWidth,
        height: pauseRectangleHeight,
        fill: '#00897b'
      });
      this._pauseLayer.add(leftPauseRectangle);

      var rightPauseRectangle = new Konva.Rect({
        x: this._width / 2 + offsetX,
        y: (this._height - pauseRectangleHeight) / 2,
        width: pauseRectangleWidth,
        height: pauseRectangleHeight,
        fill: '#00897b'
      });
      this._pauseLayer.add(rightPauseRectangle);

      this._zoomStage.draw();
    };

    /**
     * @description
     * Un-pauses the screen sharing and removes the pause layer from the canvas.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#unpause
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.unpause = function() {
      AvayaClientServices.Base.Logger.info(
        AvayaClientServices.Renderer.LoggerTags.KONVA_CONTENT_SHARING_RENDERER,
        'Screen sharing un-paused.'
      );
      this._state =
        AvayaClientServices.Renderer.Konva.KonvaContentSharingState.STARTED;
      this._pauseLayer.removeChildren();
      this._zoomStage.draw();
    };

    /**
     * @description
     * Scales sharing content and makes it smaller or bigger, depending on the scale parameter.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#zoom
     * @param {number} scale Minimal value is 0. Default value is 1.
     * Value between 0 and 1 makes content smaller. Value higher than 1 makes content bigger.
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.zoom = function(scale) {
      if (this._isContentSharingStopped()) {
        return;
      }

      if (scale <= 0) {
        throw new Error('Scale cannot be lower than 0.');
      }

      // In theory, this is allowed, but in practice it makes no sense.
      // Must be fixed as well on the client, autofit should only be applied to dimensioned elements
      // The result of dividing by zero
      if (scale >= Number.POSITIVE_INFINITY) {
        return;
      }

      this._scale = scale;
      this._zoomStage.height(this._height * this._scale);
      this._zoomStage.width(this._width * this._scale);
      this._zoomStage.scale({
        x: this._scale,
        y: this._scale
      });
      this._zoomStage.draw();
    };

    /**
     * @description
     * Scales the sharing content to the maximum width or height of the parent container.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#autoFit
     * @param {number} width - Width of the content sharing parent container in pixels.
     * @param {number} height - Height of the content sharing parent container in pixels.
     * @returns {number} scale
     */
    KonvaContentSharingRenderer.prototype.autoFit = function(width, height) {
      AvayaClientServices.Base.Logger.debug(
        AvayaClientServices.Renderer.LoggerTags.KONVA_CONTENT_SHARING_RENDERER,
        'Updating autofit to width: ' + width + ' height: ' + height
      );
      var scale;

      if ((this._width * height) / this._height < width) {
        scale = height / this._height;
      } else {
        scale = width / this._width;
      }

      this.zoom(scale);
      return scale;
    };

    /**
     * @description For debug purposes only.   Saves a frame to disk.   Requires FileSaver.js to be installed.
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#_saveFrameToDisk
     * @param {AvayaClientServices.Services.Collaboration.Frame} frame Frame that is going to be saved.
     * @private
     */
    KonvaContentSharingRenderer.prototype._saveFrameToDisk = function(frame) {
      var bitmaps = frame.getBitmaps();
      var bitmapCounter = 1;
      var zip = new JSZip(); // jshint ignore:line
      bitmaps.forEach(function(bitmap) {
        var imageType = bitmap._imageType === 0 ? 'jpeg' : 'png';
        var fileName =
          bitmapCounter++ +
          '_' +
          bitmap.getXOffset() +
          '_' +
          bitmap.getYOffset() +
          '_' +
          bitmap.getWidth() +
          '_' +
          bitmap.getHeight() +
          '.' +
          imageType;
        zip.file(fileName, bitmap.getRawBitmapData(), { base64: true });
      });
      zip.generateAsync({ type: 'blob' }).then(function(blob) {
        // requires <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.js"></script>
        saveAs(blob, 'download.zip'); // jshint ignore:line
      });
    };

    /**
     * @description
     * Performance monitor, monitor the load of the frames processing, and reduce the image quality if needed.
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#performanceMonitorStart
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.performanceMonitorStart = function() {
      var totalFrames = 0;
      var framesArray = [];
      var measurementsCount = 0;
      var config = this._config;
      var maxMeasurementsCount =
        config.konvaSharingMeasurementsWindowDuration /
        config.konvaSharingMeasurementsInterval;
      AvayaClientServices.Base.Logger.info(
        AvayaClientServices.Renderer.LoggerTags.KONVA_CONTENT_SHARING_RENDERER,
        'Starting counting frames averages.'
      );
      this._performanceMonitor = setInterval(
        function() {
          var currentFramesLength = this._queuedFrames.length;
          measurementsCount++;
          totalFrames += currentFramesLength;
          framesArray.push(currentFramesLength);
          if (measurementsCount > maxMeasurementsCount) {
            var firstFrame = framesArray.shift();
            totalFrames -= firstFrame;
          }
          if (measurementsCount >= maxMeasurementsCount) {
            var framesAverages = totalFrames / maxMeasurementsCount;
            if (config.konvaSharingDebugMode === true) {
              AvayaClientServices.Base.Logger.debug(
                AvayaClientServices.Renderer.LoggerTags
                  .KONVA_CONTENT_SHARING_RENDERER,
                'Counting frames averages, current step: ' +
                  measurementsCount +
                  ', frames averages: ' +
                  framesAverages
              );
            }
            if (currentFramesLength > config.konvaSharingFramesWarningLimit) {
              this._contentSharing._notifyPerformanceMonitorAlert(
                AvayaClientServices.Base.PerformanceAlertType.CPU_USAGE_WARNING
              );
            }
            if (framesAverages > config.konvaSharingFramesAvarageLimit) {
              var reason = 'Average frames queued is ' + framesAverages;
              this._contentSharing._notifyPerformanceMonitorAlert(
                AvayaClientServices.Base.PerformanceAlertType
                  .CPU_USAGE_OVERLOADED,
                reason
              );
            }
          }
        }.bind(this),
        config.konvaSharingMeasurementsInterval * 1000
      );
    };

    /**
     * @description
     * stops the performance monitor, and clear the interval
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer#performanceMonitorStop
     * @returns {void}
     */
    KonvaContentSharingRenderer.prototype.performanceMonitorStop = function() {
      if (this._performanceMonitor) {
        clearInterval(this._performanceMonitor);
        this._performanceMonitor = undefined;
        AvayaClientServices.Base.Logger.info(
          AvayaClientServices.Renderer.LoggerTags
            .KONVA_CONTENT_SHARING_RENDERER,
          'Konva Renderer performance monitor was stopped.'
        );
      }
    };

    AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer = KonvaContentSharingRenderer;
  })(AvayaClientServices);
  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */

  (function(AvayaClientServices) {
    'use strict';

    /**
     * @description
     * Determines content sharing statuses.
     * @readonly
     * @enum {String}
     * @memberOf AvayaClientServices.Renderer.Konva
     * @define AvayaClientServices.Renderer.Konva.KonvaContentSharingState
     */
    var KonvaContentSharingState = {
      /**
       * Indicates that content sharing is currently started.
       * */
      STARTED: 'STARTED',
      /**
       * Indicates that content sharing is currently stopped.
       * */
      STOPPED: 'STOPPED',
      /**
       * Indicates that content sharing is currently paused.
       * */
      PAUSED: 'PAUSED'
    };

    AvayaClientServices.Renderer.Konva.KonvaContentSharingState = KonvaContentSharingState;
  })(AvayaClientServices);
  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */

  (function() {
    'use strict';

    /**
     * @description
     * KonvaWhiteboardConverter provides functionality for converting Konva.Shape into SDK.Shape, which is
     * specially prepared object with all the necessary data.
     *
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaWhiteboardConverter
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaWhiteboardConverter() {}

    KonvaWhiteboardConverter.prototype =
      /** @lends AvayaClientServices.Renderer.Konva.KonvaWhiteboardConverter.prototype **/
      {
        /**
         * @description
         * Converts Konva rectangle shape into SDK shape object.
         * @public
         * @function
         * @param {Konva.Rect} konvaShape Konva shape which is going to be converted to the SDK shape.
         * @returns {AvayaClientServices.Services.Collaboration.Shape}
         */
        convertRectangle: function(konvaShape) {
          var ownerDisplayName = '';
          var filled = !!konvaShape.attrs.fill;
          var isMine = true;
          var color = this._colorHexToInt(
            filled ? konvaShape.attrs.fill : konvaShape.attrs.stroke
          );
          var width = konvaShape.attrs.strokeWidth || 0;
          var alpha;
          var sdkShape = new AvayaClientServices.Services.Collaboration.Shape(
            ownerDisplayName,
            filled,
            isMine,
            color,
            alpha,
            width
          );
          sdkShape.addMovePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.x,
              konvaShape.attrs.y
            )
          );
          sdkShape.addLinePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.x + konvaShape.attrs.width,
              konvaShape.attrs.y
            )
          );
          sdkShape.addLinePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.x + konvaShape.attrs.width,
              konvaShape.attrs.y + konvaShape.attrs.height
            )
          );
          sdkShape.addLinePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.x,
              konvaShape.attrs.y + konvaShape.attrs.height
            )
          );
          sdkShape.addLinePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.x,
              konvaShape.attrs.y
            )
          );
          sdkShape.finishDrawing();
          return sdkShape;
        },

        /**
         * @description
         * Converts Konva ellipse shape into SDK shape object.
         * @public
         * @function
         * @param {Konva.Ellipse} konvaShape Konva shape which is going to be converted to the SDK shape.
         * @returns {AvayaClientServices.Services.Collaboration.Circle}
         */
        convertEllipse: function(konvaShape) {
          var topLeft = new AvayaClientServices.Services.Collaboration.Point(
            konvaShape.attrs.x - konvaShape.attrs.radiusX,
            konvaShape.attrs.y - konvaShape.attrs.radiusY
          );
          var bottomRight = new AvayaClientServices.Services.Collaboration.Point(
            konvaShape.attrs.x + konvaShape.attrs.radiusX,
            konvaShape.attrs.y + konvaShape.attrs.radiusY
          );
          var ownerDisplayName = '';
          var filled = !!konvaShape.attrs.fill;
          var isMine = true;
          var color = this._colorHexToInt(
            filled ? konvaShape.attrs.fill : konvaShape.attrs.stroke
          );
          var width = konvaShape.attrs.strokeWidth || 0;
          var alpha;
          var sdkShape = new AvayaClientServices.Services.Collaboration.Circle(
            topLeft,
            bottomRight,
            ownerDisplayName,
            filled,
            isMine,
            color,
            alpha,
            width
          );
          return sdkShape;
        },

        /**
         * @description
         * Converts Konva text into SDK shape object.
         * @public
         * @function
         * @param {Konva.Text} konvaShape Konva shape which is going to be converted to the SDK shape.
         * @returns {AvayaClientServices.Services.Collaboration.WhiteboardText}
         */
        convertText: function(konvaShape) {
          var content = konvaShape.attrs.text;
          var fontSize = konvaShape.attrs.fontSize;
          var ownerDisplayName = '';
          var filled = true;
          var isMine = true;
          var color = this._colorHexToInt(konvaShape.attrs.fill);
          var sdkShape = new AvayaClientServices.Services.Collaboration.WhiteboardText(
            content,
            fontSize,
            ownerDisplayName,
            filled,
            isMine,
            color
          );
          sdkShape.setPosition(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.x,
              konvaShape.attrs.y
            )
          );
          return sdkShape;
        },

        /**
         * @description
         * Converts Konva line shape into SDK shape object.
         * @public
         * @function
         * @param {Konva.Line} konvaShape Konva shape which is going to be converted to the SDK shape.
         * @returns {AvayaClientServices.Services.Collaboration.Shape}
         */
        convertLine: function(konvaShape) {
          var ownerDisplayName = '';
          var filled = false;
          var isMine = true;
          var color = this._colorHexToInt(konvaShape.attrs.stroke);
          var width = konvaShape.attrs.strokeWidth || 0;
          var alpha;
          var sdkShape = new AvayaClientServices.Services.Collaboration.Shape(
            ownerDisplayName,
            filled,
            isMine,
            color,
            alpha,
            width
          );
          sdkShape.addMovePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.points[0],
              konvaShape.attrs.points[1]
            )
          );
          sdkShape.addLinePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape.attrs.points[2],
              konvaShape.attrs.points[3]
            )
          );
          sdkShape.finishDrawing();
          return sdkShape;
        },

        /**
         * @description
         * Converts Konva polygon mark into SDK shape object.
         * @public
         * @function
         * @param {Konva.Shape} konvaShape Konva shape which is going to be converted to the SDK shape.
         * @returns {AvayaClientServices.Services.Collaboration.Shape}
         */
        convertPolygon: function(konvaShape, isFinished) {
          var ownerDisplayName = '';
          var filled = !!konvaShape.attrs.fill;
          var isMine = true;
          var color = this._colorHexToInt(
            filled ? konvaShape.attrs.fill : konvaShape.attrs.stroke
          );
          var width = konvaShape.attrs.strokeWidth || 0;
          var alpha = konvaShape.attrs.opacity * 100;
          var sdkShape = new AvayaClientServices.Services.Collaboration.Shape(
            ownerDisplayName,
            filled,
            isMine,
            color,
            alpha,
            width
          );
          sdkShape.addMovePoint(
            new AvayaClientServices.Services.Collaboration.Point(
              konvaShape._modelStart.x,
              konvaShape._modelStart.y
            )
          );
          var i = 0;
          for (i; i < konvaShape._modelRelativePoints.length; i++) {
            var point = konvaShape._modelRelativePoints[i];
            if (point.x !== 0 && point.y !== 0) {
              sdkShape.addLinePoint(
                new AvayaClientServices.Services.Collaboration.Point(
                  konvaShape._modelStart.x + point.x,
                  konvaShape._modelStart.y + point.y
                )
              );
            }
          }
          if (isFinished) {
            sdkShape.finishDrawing();
          }

          return sdkShape;
        },

        /**
         * Helper function converting color value as string (beginning with #) to number value.
         *
         * @private
         * @function
         * @param {string} color string value -  e.g. "#FFFFFF"
         * @returns {number} color number value - e.g. 16777215
         */
        _colorHexToInt: function(colorString) {
          if (colorString[0] === '#') {
            return parseInt('0x'.concat(colorString.slice(1)));
          } else {
            return parseInt('0x'.concat(colorString));
          }
        }
      };

    AvayaClientServices.Renderer.Konva.KonvaWhiteboardConverter = KonvaWhiteboardConverter;
  })();
  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */

  (function(AvayaClientServices) {
    'use strict';

    /**
     * @description
     * KonvaWhiteboardRenderer extends Collaboration module WhiteboardRenderer.
     * Provides functionality for rendering shapes on the whiteboard canvas.
     *
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer
     * @memberOf AvayaClientServices.Renderer.Konva
     * @extends AvayaClientServices.Services.Collaboration.WhiteboardRenderer
     * @constructor
     */
    function KonvaWhiteboardRenderer() {
      AvayaClientServices.Services.Collaboration.WhiteboardRenderer.call(this);

      /**
       *
       * @type {number}
       * @private
       */
      this._width = 800;

      /**
       *
       * @type {number}
       * @private
       */
      this._height = 600;

      /**
       *
       * @type {number}
       * @private
       */
      this._scale = 1;

      /**
       *
       * @type {Konva.Stage}
       * @private
       */
      this._drawingStage = undefined;

      /**
       *
       * @type {Konva.Layer}
       * @private
       */
      this._drawingLayer = undefined;

      /**
       *
       * @type {Canvas}
       * @private
       */
      this._canvas = undefined;

      /**
       *
       * @type {string}
       * @private
       */
      this._mode =
        AvayaClientServices.Services.Collaboration.WhiteboardToolModes.STROKE;

      /**
       *
       * @type {string}
       * @private
       */
      this._color = '#211e1e';

      /**
       *
       * @type {number}
       * @private
       */
      this._strokeWidth = 6;

      /**
       *
       * @type {AvayaClientServices.Renderer.Konva.KonvaWhiteboardConverter}
       * @private
       */
      this._whiteboardConverter = new AvayaClientServices.Renderer.Konva.KonvaWhiteboardConverter();

      /**
       *
       * @type {AvayaClientServices.Renderer.Konva.KonvaWhiteboardTools}
       * @private
       */
      this._whiteboardTools = new AvayaClientServices.Renderer.Konva.KonvaWhiteboardTools(
        this
      );

      /**
       *
       * @type {{}}
       * @private
       */
      this._tool = this._whiteboardTools[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.RECTANGLE
      ];

      /**
       *
       * @type {boolean}
       * @private
       */
      this._isDrawing = false;

      /**
       *
       * @type {number}
       * @private
       */
      this._fontSize = 19;

      /**
       *
       * @type {object}
       * @private
       */
      this._annotatedShape = undefined;

      /**
       *
       * @type {boolean}
       * @private
       */
      this._inputExists = false;

      /**
       *
       * @type {object}
       * @private
       */
      this._curWin = window;

      /**
       *
       * @type {object}
       * @private
       */
      this._textAreaData = undefined;
    }

    KonvaWhiteboardRenderer.prototype = Object.create(
      AvayaClientServices.Services.Collaboration.WhiteboardRenderer.prototype
    );

    /**
     * @description
     * Returns width of the stage
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#getWidth
     * @returns {Number}
     */
    KonvaWhiteboardRenderer.prototype.getWidth = function() {
      return this._width;
    };

    /**
     * @description
     * Returns height of the stage
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#getHeight
     * @returns {Number}
     */
    KonvaWhiteboardRenderer.prototype.getHeight = function() {
      return this._height;
    };

    /**
     * @description
     * Sets up and starts the whiteboard canvas.
     * Depending on the selected tool, calls the appropriate function on mouse click event.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#start
     * @param {string} containerId ID of DOM element where the whiteboard canvas will be append to.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.start = function(containerId) {
      this._setupWhiteboard(containerId);

      // TODO: pass canvas in more elegant way
      this._canvas = document
        .getElementById(containerId)
        .getElementsByClassName('konvajs-content')[0]
        .getElementsByTagName('canvas')[0];

      this._canvas.onmousedown = function(e) {
        if (!this._whiteboard.getDrawingCapability().isAllowed) {
          AvayaClientServices.Base.Logger.warn(
            AvayaClientServices.Base.LoggerTags.COLLABORATION_SERVICE,
            'It is not possible to use any whiteboard tool since whiteboard is not available at the moment.'
          );
          return false;
        } else {
          switch (this._tool.type) {
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .SELECTION:
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .RECTANGLE:
              this._dragTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .CIRCLE:
              this._dragTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .MARKER:
              this._liveTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .PEN:
              this._liveTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .LINE:
              this._dragTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .STAMP:
              this._stampTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .TEXT:
              this._textTool(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .DELETE:
              this._deleteShape(e);
              return;
            case AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .CLEAR:
              return;
          }
        }
      }.bind(this);
    };

    /**
     * @public
     * @param {array} points
     * @param {object} point
     * @return {boolean}
     */
    function sift(points, point) {
      function arePointsInLine(point1, point2, point3) {
        var lineObj = {};
        lineObj.gradient =
          (point1.point.getY() - point2.point.getY()) /
          (point1.point.getX() - point2.point.getX());
        lineObj.interceptY =
          point1.point.getY() - lineObj.gradient * point1.point.getX();

        var a =
          point3.point.getY() ===
          lineObj.gradient * point3.point.getX() + lineObj.interceptY;
        var b =
          (point1.point.getX() === point2.point.getX() &&
            point2.point.getX() === point3.point.getX()) ||
          (point1.point.getY() === point2.point.getY() &&
            point2.point.getY() === point3.point.getY());
        return (a && (lineObj.gradient === 1 || lineObj.gradient === -1)) || b;
      }

      if (points.length > 1) {
        var firstPoint = points.length - 2,
          secondPoint = points.length - 1;
        var isInline = arePointsInLine(
          points[firstPoint],
          points[secondPoint],
          point
        );
        if (isInline) {
          points.splice(secondPoint, 1);
        }
      }
    }

    /**
     * @description
     * Ends the whiteboard service and clears all the HTML canvas elements.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#end
     * @param {string} containerId ID of DOM element with the whiteboard canvas which is going to be removed.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.end = function(containerId) {
      if (this._drawingLayer) {
        this._canvas.onmousedown = undefined;
        this._drawingLayer.destroyChildren();
        this._drawingStage.destroyChildren();
        this._drawingLayer.destroy();
        this._drawingStage.destroy();
        document.getElementById(containerId).innerHTML = '';
        this._drawingLayer = undefined;
        this._drawingStage = undefined;
      }
    };

    /**
     * @description
     * Draws a given shape on the whiteboard canvas.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#drawShape
     * @param {AvayaClientServices.Services.Collaboration.AbstractShape} shape Shape that is going to be drawn on the whiteboard canvas.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.drawShape = function(shape) {
      if (this._drawingLayer) {
        var konvaShape;
        var shapeAlreadyExists = this._drawingLayer.children.filter(function(
          s
        ) {
          return s.id === shape.getId();
        }).length;

        if (shapeAlreadyExists) {
          var sdkShape = this._whiteboard.getActiveSurface().getShapes()[
            shape.getId()
          ];
          if (
            !AvayaClientServices.Base.Utils.isDefined(
              sdkShape._ownerDisplayName
            ) ||
            sdkShape._ownerDisplayName === ''
          ) {
            sdkShape._ownerDisplayName = shape.getOwnerDisplayName();
          }
          return;
        }
        if (shape instanceof AvayaClientServices.Services.Collaboration.Shape) {
          konvaShape = this._createShape(shape);
        } else if (
          shape instanceof AvayaClientServices.Services.Collaboration.Circle
        ) {
          konvaShape = this._createCircle(shape);
        } else if (
          shape instanceof
          AvayaClientServices.Services.Collaboration.WhiteboardText
        ) {
          konvaShape = this._createText(shape);
        } else {
          return;
        }

        this._drawingLayer.add(konvaShape);
        this._drawingLayer.draw();
      }
    };

    /**
     * @description
     * Removes a given shape from the whiteboard canvas.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#removeShape
     * @param {AvayaClientServices.Services.Collaboration.AbstractShape} shape Shape that is going to be removed from the whiteboard canvas.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.removeShape = function(shape) {
      var konvaShape = this._getKonvaShape(shape);
      if (konvaShape) {
        konvaShape.destroy();
        this._drawingLayer.draw();
      }
    };

    /**
     * Function find clicked shape and delete it from whiteboard canvas if shape is our.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_deleteShape
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Object} event
     * @return {AvayaClientServices.Base.Promise}
     */
    KonvaWhiteboardRenderer.prototype._deleteShape = function(event) {
      var konvaShape = this._drawingLayer.getIntersection({
        x: event.offsetX,
        y: event.offsetY
      });
      if (konvaShape !== null) {
        var sdkShape = this._getSDKShape(konvaShape);
        if (sdkShape.isMine()) {
          this.removeShape(sdkShape);
          return this._whiteboard.getActiveSurface().removeShape(sdkShape);
        }
      }
      var dfd = $.Deferred();
      dfd.reject();
      return dfd.promise();
    };

    /**
     * @description
     * Updates the shape position, when the shape is moved by the selection tool.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#updateShape
     * @param {AvayaClientServices.Services.Collaboration.AbstractShape} shape Shape that is going to be updated.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.updateShape = function(shape) {
      var konvaShape = this._getKonvaShape(shape);
      if (konvaShape) {
        if (shape.getId() !== konvaShape.id) {
          konvaShape.id = shape.getId();
        }
      }
      var x, y;
      if (
        konvaShape instanceof Konva.Ellipse &&
        AvayaClientServices.Base.Utils.isDefined(shape.getTranslation())
      ) {
        x = konvaShape.initX + shape.getTranslation().getX();
        y = konvaShape.initY + shape.getTranslation().getY();
      } else if (
        konvaShape instanceof Konva.Text &&
        AvayaClientServices.Base.Utils.isDefined(shape.getPosition())
      ) {
        x = shape.getPosition().getX() + shape.getTranslation().getX();
        y = shape.getPosition().getY() + shape.getTranslation().getY();
      } else if (konvaShape instanceof Konva.Shape) {
        if (AvayaClientServices.Base.Utils.isDefined(shape.getTranslation())) {
          if (konvaShape instanceof Konva.Rect) {
            x =
              shape.getPoints()[0].point.getX() + shape.getTranslation().getX();
            y =
              shape.getPoints()[0].point.getY() + shape.getTranslation().getY();
          } else {
            x = shape.getTranslation().getX();
            y = shape.getTranslation().getY();
          }
        } else {
          konvaShape.attrs.sceneFunc = function(context) {
            context.beginPath();
            var points = shape.getPoints();
            for (var i = 0; i < points.length; i++) {
              if (points[i].isMove) {
                context.moveTo(points[i].point.getX(), points[i].point.getY());
              } else {
                context.lineTo(points[i].point.getX(), points[i].point.getY());
              }
            }
            context.fillStrokeShape(konvaShape);
          }.bind(this);
        }
      }

      if (
        AvayaClientServices.Base.Utils.isDefined(x) &&
        AvayaClientServices.Base.Utils.isDefined(y)
      ) {
        konvaShape.position({ x: x, y: y });
      }
      this._drawingLayer.draw();
    };

    /**
     * @description
     * Deletes all the shapes from the whiteboard canvas.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#clearContent
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.clearContent = function() {
      if (AvayaClientServices.Base.Utils.isDefined(this._drawingLayer)) {
        for (var i = this._drawingLayer.children.length - 1; i >= 0; i--) {
          this._drawingLayer.children[i].destroy();
        }
        this._drawingLayer.draw();
      }
    };

    /**
     * @description
     * Allows to select and set the whiteboard tool and tool mode (if exists).
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#setTool
     * @param {AvayaClientServices.Services.Collaboration.WhiteboardToolTypes} tool Tool that is going to be selected.
     * @param {string} mode Tool mode: filled or stroke.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.setTool = function(tool, mode) {
      AvayaClientServices.Base.Logger.debug(
        AvayaClientServices.Renderer.LoggerTags.KONVA_WHITEBOARD_RENDERER,
        'Tool set: ',
        tool,
        mode
      );

      if (
        this._tool.type ===
          AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
            .SELECTION &&
        this._drawingLayer
      ) {
        this._unsetDraggable();
      }

      if (
        this._tool.type ===
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.TEXT
      ) {
        this._destructTextTool();
      }

      this._tool = this._whiteboardTools[tool];
      if (mode && this._tool.modes[mode]) {
        this._mode = mode;
      }

      if (
        AvayaClientServices.Base.Utils.isDefined(this._canvas) &&
        tool !==
          AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
            .SELECTION &&
        tool !==
          AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.DELETE
      ) {
        this._canvas.onmousemove = undefined;
        this._canvas.onmouseout = undefined;
        if (AvayaClientServices.Base.Utils.isDefined(this._annotatedShape)) {
          this._onShowShapeAnnotationCallbacks.fire();
          this._annotatedShape = undefined;
        }
      }

      if (
        AvayaClientServices.Base.Utils.isDefined(this._canvas) &&
        (tool ===
          AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
            .SELECTION ||
          tool ===
            AvayaClientServices.Services.Collaboration.WhiteboardToolTypes
              .DELETE)
      ) {
        this._canvas.onmousemove = function(event) {
          var konvaShape = this._drawingLayer.getIntersection({
            x: event.offsetX,
            y: event.offsetY
          });
          if (
            (konvaShape !== null &&
              !AvayaClientServices.Base.Utils.isDefined(
                this._annotatedShape
              )) ||
            (konvaShape !== null &&
              AvayaClientServices.Base.Utils.isDefined(this._annotatedShape) &&
              konvaShape._id !== this._annotatedShape._id)
          ) {
            var sdkShape = this._getSDKShape(konvaShape);
            this._onShowShapeAnnotationCallbacks.fire(sdkShape);
            this._annotatedShape = konvaShape;
          } else if (
            konvaShape === null &&
            AvayaClientServices.Base.Utils.isDefined(this._annotatedShape)
          ) {
            this._onShowShapeAnnotationCallbacks.fire();
            this._annotatedShape = undefined;
          }
        }.bind(this);
        this._canvas.onmouseout = function(e) {
          if (AvayaClientServices.Base.Utils.isDefined(this._annotatedShape)) {
            this._onShowShapeAnnotationCallbacks.fire();
            this._annotatedShape = undefined;
          }
        }.bind(this);
      }

      if (
        tool ===
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.SELECTION
      ) {
        this._setDraggable();
      }

      if (
        tool ===
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.CLEAR
      ) {
        // Notice: Here you can ask user to confirm deletion
        this._clear();
      }
    };

    /**
     * Delete textarea, draw its text on canvas if any.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_destructTextTool
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._destructTextTool = function() {
      var textInput = this._canvas.parentNode.querySelector(
        '#' + this._getTextInputId()
      );

      if (textInput) {
        if (textInput.value) {
          this._renderText(undefined, textInput);
        } else {
          textInput.parentNode.removeChild(textInput);
        }
        this._textAreaData = undefined;
      }
    };

    /**
     * Lookup for ID used to create input element for Text Tool.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_getTextInputId
     * @returns {AvayaClientServices.Renderer.Konva.KonvaTextTool}
     */
    KonvaWhiteboardRenderer.prototype._getTextInputId = function() {
      var textInput = this._whiteboardTools[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.TEXT
      ];

      return textInput._getElementId();
    };

    /**
     * Draw input on canvas.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_renderText
     * @param {Object} event
     * @param {Element} textInput
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._renderText = function(event, textInput) {
      var konvaText = this._tool.endInput(
        event,
        this._canvas,
        textInput,
        this._scale
      );
      konvaText.fill(this._color);
      konvaText.fontSize(
        AvayaClientServices.Services.Collaboration.WhiteboardTextSizes[
          this._mode
        ]
      );
      konvaText.fontStyle('bold');
      this._drawingLayer.add(konvaText);
      this._drawingLayer.draw();
      this._convertAndAddShape(konvaText, this._tool.name);
    };

    /**
     * @description
     * Allows to select and set a color for the whiteboard tool.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#setColor
     * @param {string} cl Hexadecimal color.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.setColor = function(cl) {
      AvayaClientServices.Base.Logger.debug(
        AvayaClientServices.Renderer.LoggerTags.KONVA_WHITEBOARD_RENDERER,
        'Color set: ',
        cl
      );
      this._color = cl;
    };

    /**
     * @description
     * Scales canvas, making the whiteboard smaller or bigger, depending on the scale parameter.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#zoom
     * @param {number} scale Minimal value is 0. Default value is 1.
     * Value between 0 and 1 makes canvas smaller. Value higher than 1 makes canvas bigger.
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype.zoom = function(scale) {
      if (scale <= 0) {
        throw new Error('Scale cannot be lower than 0.');
      }

      // In theory, this is allowed, but in practice it makes no sense.
      // Must be fixed as well on the client, autofit should only be applied to dimensioned elements
      // The result of dividing by zero
      if (scale >= Number.POSITIVE_INFINITY) {
        return;
      }

      this._scale = scale;
      this._drawingLayer.scale({
        x: this._scale,
        y: this._scale
      });
      this._drawingLayer.draw();
      this._drawingStage.height(this._height * this._scale);
      this._drawingStage.width(this._width * this._scale);
      this._zoomTool();
    };

    KonvaWhiteboardRenderer.prototype._zoomTool = function() {
      if (
        this._tool.type ===
          AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.TEXT &&
        this._canvas.parentNode
      ) {
        var textarea = this._canvas.parentNode.querySelector(
          '#' + this._getTextInputId()
        );
        if (textarea) {
          this._textAreaData.textTool._zoom(
            textarea,
            this._textAreaData,
            this._canvas,
            this._mode,
            this._scale
          );
        }
      }
    };

    /**
     * @description
     * Resubscribe current window on mouse events if it has been changed
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_resubscribeWinEvents
     */
    KonvaWhiteboardRenderer.prototype.resubscribeWinEvents = function() {
      var newWin = this._canvas.ownerDocument.defaultView;

      if (newWin && this._curWin !== newWin) {
        if (Konva.DD._endDragBefore) {
          this._curWin.removeEventListener(
            'mouseup',
            Konva.DD._endDragBefore,
            true
          );
          this._curWin.removeEventListener(
            'touchend',
            Konva.DD._endDragBefore,
            true
          );
          newWin.addEventListener('mouseup', Konva.DD._endDragBefore, true);
          newWin.addEventListener('touchend', Konva.DD._endDragBefore, true);
        }

        if (Konva.DD._drag) {
          this._curWin.removeEventListener('mousemove', Konva.DD._drag);
          this._curWin.removeEventListener('touchmove', Konva.DD._drag);
          newWin.addEventListener('mousemove', Konva.DD._drag);
          newWin.addEventListener('touchmove', Konva.DD._drag);
        }

        if (Konva.DD._endDragAfter) {
          this._curWin.removeEventListener(
            'mouseup',
            Konva.DD._endDragAfter,
            false
          );
          this._curWin.removeEventListener(
            'touchend',
            Konva.DD._endDragAfter,
            false
          );
          newWin.addEventListener('mouseup', Konva.DD._endDragAfter, false);
          newWin.addEventListener('touchend', Konva.DD._endDragAfter, false);
        }
        if (this._drawingStage && this._drawingStage._bindContentEvents) {
          this._drawingStage._bindContentEvents();
        }
        this._curWin = newWin;
      }
    };

    /**
     * @description
     * Scales the whiteboard to the maximum width or height of the parent container.
     *
     * @public
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#autoFit
     * @param {number} width - Width of the whiteboard parent container in pixels.
     * @param {number} height - Height of the hiteboard parent container in pixels.
     * @returns {number} scale
     */
    KonvaWhiteboardRenderer.prototype.autoFit = function(width, height) {
      var scale;

      if ((this._width * height) / this._height < width) {
        scale = height / this._height;
      } else {
        scale = width / this._width;
      }

      this.zoom(scale);
      return scale;
    };

    /**
     * Function find clicked place and draw there text shape (Text tool).
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_textTool
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Object} event
     * @returns {void}
     */

    KonvaWhiteboardRenderer.prototype._textTool = function(event) {
      var textInput = this._canvas.parentNode.querySelector(
        '#' + this._getTextInputId()
      );

      if (textInput) {
        if (this._inputExists) {
          this._renderText(event, textInput);
        }
      } else {
        this._textAreaData = this._tool.startInput(
          event,
          this._canvas,
          this._mode,
          this._scale
        );
        this._inputExists = true;
      }
    };

    /**
     * Function find clicked place and draw there drag shapes (Rectangle tool, Ellipse tool).
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_dragTool
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Object} event
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._dragTool = function(event) {
      var konvaShape;
      this._isDrawing = true;
      konvaShape = this._tool.startFunction(event, this._mode, this._scale);

      var handleEndDrawing = function handleEndDrawing(event) {
        this._isDrawing = false;
        this._tool.endFunction(event);
        this._clearMouseEvents();
        konvaShape.isMine = true;
        this._convertAndAddShape(konvaShape, this._tool.name);
      }.bind(this);

      this._canvas.onmouseup = function(event) {
        handleEndDrawing(event);
      }.bind(this);

      this._canvas.onmouseleave = function(event) {
        handleEndDrawing(event);
      }.bind(this);
    };

    /**
     * Function find clicked place and draw there liveEvent shapes (Pen tool, Marker tool).
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_liveTool
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Object} event
     * @return {object}
     */
    KonvaWhiteboardRenderer.prototype._liveTool = function(event) {
      var konvaShape;
      this._isDrawing = true;
      konvaShape = this._tool.startFunction(event, this._scale);
      var sdkShape = this._whiteboardConverter.convertPolygon(
        konvaShape,
        false
      );

      var handleEndDrawing = function handleEndDrawing(event) {
        this._isDrawing = false;
        this._tool.endFunction(event);
        this._clearMouseEvents();
        konvaShape.isMine = true;
        if (!sdkShape.isFinished()) {
          sdkShape.finishDrawing().done(
            function(newId) {
              this._updateShapeId(konvaShape, newId);
            }.bind(this)
          );
        }
      }.bind(this);

      this._canvas.onmousemove = function(event) {
        this._tool.drawFunction(event, konvaShape);
        var newPoint = new AvayaClientServices.Services.Collaboration.Point(
          event.offsetX / this._scale,
          event.offsetY / this._scale
        );
        sift(sdkShape.getPoints(), { point: newPoint });
        sdkShape.addLinePoint(newPoint);
      }.bind(this);

      this._canvas.onmouseup = function(event) {
        handleEndDrawing(event);
      }.bind(this);

      this._canvas.onmouseleave = function(event) {
        handleEndDrawing(event);
      }.bind(this);

      return this._whiteboard
        .getActiveSurface()
        .addShape(sdkShape)
        .done(
          function(newId) {
            this._updateShapeId(konvaShape, newId);
          }.bind(this)
        );
    };

    /**
     * Function find clicked place and draw there stamp shapes (Stamp tool).
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_stampTool
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Object} event
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._stampTool = function(event) {
      var konvaShape;
      this._isDrawing = true;
      konvaShape = this._tool.drawFunction(event, this._scale);
      konvaShape.isMine = true;
      this._isDrawing = false;
      this._convertAndAddShape(konvaShape, this._tool.name);
    };

    /**
     * Function clear all active whiteboard shapes and send that event to the server.
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_clear
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @returns {AvayaClientServices.Base.Promise}
     */
    KonvaWhiteboardRenderer.prototype._clear = function() {
      this.clearContent();
      return this._whiteboard.getActiveSurface().clearContent();
    };

    /**
     * Function create Konva.Shape from SDK Shape object.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_createShape
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.Shape} shape
     * @returns {Konva.Shape}
     */
    KonvaWhiteboardRenderer.prototype._createShape = function(shape) {
      var points = shape.getPoints();
      var konvaShape = new Konva.Shape({
        fill: shape.isFilled()
          ? AvayaClientServices.Base.Utils.colorIntToHex(shape.getColor())
          : undefined,
        stroke: shape.isFilled()
          ? undefined
          : AvayaClientServices.Base.Utils.colorIntToHex(shape.getColor()),
        strokeWidth: shape.isFilled() ? undefined : shape.getWidth(),
        lineCap: 'round',
        lineJoin: 'round',
        opacity: shape.getAlpha() / 100,
        sceneFunc: function(context) {
          context.beginPath();
          var i = 0,
            point;
          for (i; i < points.length; i++) {
            point = points[i];
            if (point.isMove) {
              context.moveTo(point.point.getX(), point.point.getY());
            } else {
              //EDGEONLY JSCSDK-4181
              context.lineTo(
                point.point.getX() + 0.1,
                point.point.getY() + 0.1
              );
            }
          }
          context.fillStrokeShape(this);
        }
      });
      if (AvayaClientServices.Base.Utils.isDefined(shape.getTranslation())) {
        konvaShape.position({
          x: shape.getTranslation().getX(),
          y: shape.getTranslation().getY()
        });
      }

      return this._addCommonData(shape, konvaShape);
    };

    /**
     * Function move shape on whiteboard. This method is only called from the tool dragEnd
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_moveShape
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.Shape} konvaShape
     * @param {AvayaClientServices.Services.Collaboration.Point} translation
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._moveShape = function(
      konvaShape,
      translation
    ) {
      var sdkShape = this._getSDKShape(konvaShape);
      var translatedX = translation.newPositionX - translation.selectionX;
      var translatedY = translation.newPositionY - translation.selectionY;
      var currentTranslation = sdkShape.getTranslation();
      if (AvayaClientServices.Base.Utils.isDefined(currentTranslation)) {
        sdkShape.setTranslation(
          new AvayaClientServices.Services.Collaboration.Point(
            currentTranslation.getX() + translatedX,
            currentTranslation.getY() + translatedY
          )
        );
      } else {
        sdkShape.setTranslation(
          new AvayaClientServices.Services.Collaboration.Point(
            translatedX,
            translatedY
          )
        );
      }

      this._whiteboard.getActiveSurface().updateShape(sdkShape);
    };

    /**
     * Function create Konva.Ellipse shape from SDK Shape object.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_createCircle
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.Circle} circle
     * @returns {Konva.Ellipse}
     */
    KonvaWhiteboardRenderer.prototype._createCircle = function(circle) {
      var konvaEllipse = new Konva.Ellipse({
        x:
          circle.getTopLeft().getX() +
          (circle.getBottomRight().getX() - circle.getTopLeft().getX()) / 2,
        y:
          circle.getTopLeft().getY() +
          (circle.getBottomRight().getY() - circle.getTopLeft().getY()) / 2,
        radius: {
          x: (circle.getBottomRight().getX() - circle.getTopLeft().getX()) / 2,
          y: (circle.getBottomRight().getY() - circle.getTopLeft().getY()) / 2
        },
        fill: circle.isFilled()
          ? AvayaClientServices.Base.Utils.colorIntToHex(circle.getColor())
          : undefined,
        stroke: circle.isFilled()
          ? undefined
          : AvayaClientServices.Base.Utils.colorIntToHex(circle.getColor()),
        strokeWidth: circle.isFilled() ? undefined : circle.getWidth()
      });
      konvaEllipse.initX = konvaEllipse.attrs.x;
      konvaEllipse.initY = konvaEllipse.attrs.y;

      if (AvayaClientServices.Base.Utils.isDefined(circle.getTranslation())) {
        konvaEllipse.position({
          x: konvaEllipse.initX + circle.getTranslation().getX(),
          y: konvaEllipse.initY + circle.getTranslation().getY()
        });
      }

      return this._addCommonData(circle, konvaEllipse);
    };

    /**
     * Function create Konva.Text from SDK Shape object.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_createText
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.WhiteboardText} text
     * @returns {Konva.Text}
     */
    KonvaWhiteboardRenderer.prototype._createText = function(text) {
      var konvaText = new Konva.Text({
        x: text.getPosition().getX(),
        y: text.getPosition().getY(),
        fill: AvayaClientServices.Base.Utils.colorIntToHex(text.getColor()),
        text: text.getContent(),
        fontSize: text.getFontSize(),
        fontFamily: 'Arial',
        fontStyle: 'bold',
        lineHeight: 1.25
      });
      // konvaText.fontStyle('bold');

      if (AvayaClientServices.Base.Utils.isDefined(text.getTranslation())) {
        konvaText.position({
          x: text.getPosition().getX() + text.getTranslation().getX(),
          y: text.getPosition().getY() + text.getTranslation().getY()
        });
      }

      return this._addCommonData(text, konvaText);
    };

    /**
     * Function add common information about Konva.Shape.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_addCommonData
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.AbstractShape} shape
     * @param {Konva.Shape|Konva.Ellipse|Konva.Text} konvaShape
     * @returns {Konva.Shape|Konva.Ellipse|Konva.Text}
     */
    KonvaWhiteboardRenderer.prototype._addCommonData = function(
      shape,
      konvaShape
    ) {
      konvaShape.id = shape.getId();
      konvaShape.ownerDisplayName = shape.getOwnerDisplayName();
      konvaShape.status = shape.getStatus();
      konvaShape.isMine = shape.isMine();
      return konvaShape;
    };

    /**
     * Function setup whiteboard. Add Konva.Stage and Konva.Layer.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_setupWhiteboard
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {string} containerId
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._setupWhiteboard = function(containerId) {
      if (this._drawingStage) {
        this.end(containerId);
      }
      /**
       *
       * @type {Konva.Stage}
       * @private
       */
      this._drawingStage = new Konva.Stage({
        container: containerId,
        width: this._width,
        height: this._height
      });

      /**
       *
       * @type {Konva.Layer}
       * @private
       */
      this._drawingLayer = new Konva.Layer();
      this._drawingStage.add(this._drawingLayer);
      this.zoom(this._scale);
    };

    /**
     * Function convert Konva.Shape into SDK shape object and add shape.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_addShape
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.Shape} konvaShape
     * @returns {AvayaClientServices.Base.Promise}
     */
    KonvaWhiteboardRenderer.prototype._convertAndAddShape = function(
      konvaShape
    ) {
      var sdkShape;
      if (konvaShape instanceof Konva.Rect) {
        sdkShape = this._whiteboardConverter.convertRectangle(konvaShape);
        return this._whiteboard
          .getActiveSurface()
          .addShape(sdkShape)
          .done(
            function(newId) {
              this._updateShapeId(konvaShape, newId);
            }.bind(this)
          );
      } else if (konvaShape instanceof Konva.Ellipse) {
        sdkShape = this._whiteboardConverter.convertEllipse(konvaShape);
        return this._whiteboard
          .getActiveSurface()
          .addCircle(sdkShape)
          .done(
            function(newId) {
              this._updateShapeId(konvaShape, newId);
            }.bind(this)
          );
      } else if (konvaShape instanceof Konva.Line) {
        sdkShape = this._whiteboardConverter.convertLine(konvaShape);
        return this._whiteboard
          .getActiveSurface()
          .addShape(sdkShape)
          .done(
            function(newId) {
              this._updateShapeId(konvaShape, newId);
            }.bind(this)
          );
      } else if (konvaShape instanceof Konva.Text) {
        sdkShape = this._whiteboardConverter.convertText(konvaShape);
        return this._whiteboard
          .getActiveSurface()
          .addText(sdkShape)
          .done(
            function(newId) {
              this._updateShapeId(konvaShape, newId);
            }.bind(this)
          );
      } else if (konvaShape instanceof Konva.Shape) {
        var isFinished = true;
        sdkShape = this._whiteboardConverter.convertPolygon(
          konvaShape,
          isFinished
        );
        return this._whiteboard
          .getActiveSurface()
          .addShape(sdkShape)
          .done(
            function(newId) {
              this._updateShapeId(konvaShape, newId);
            }.bind(this)
          );
      }
    };

    /**
     * Return Konva.Shape with the same Id or temId of SDK Shape param.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_getKonvaShape
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {AvayaClientServices.Services.Collaboration.AbstractShape} shape
     * @returns {Konva.Shape|Konva.Ellipse|Konva.Text}
     */
    KonvaWhiteboardRenderer.prototype._getKonvaShape = function(shape) {
      if (this._drawingLayer && this._drawingLayer.children) {
        for (var i = 0; i < this._drawingLayer.children.length; i++) {
          if (
            this._drawingLayer.children[i].id === shape.getId() ||
            this._drawingLayer.children[i].id === shape._tempId
          ) {
            return this._drawingLayer.children[i];
          }
        }
      }
    };

    /**
     * Return SDK Shape with that same id like Konva.Shape param.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_getSDKShape
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Konva.Shape|Konva.Text|Konva.Ellipse} konvaShape
     * @returns {AvayaClientServices.Services.Collaboration.AbstractShape}
     */
    KonvaWhiteboardRenderer.prototype._getSDKShape = function(konvaShape) {
      return this._whiteboard.getActiveSurface().getShapes()[konvaShape.id];
    };

    /**
     * Function update shape id.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_updateShapeId
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @param {Konva.Shape|Konva.Text|Konva.Ellipse} konvaShape
     * @param {number} newId
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._updateShapeId = function(
      konvaShape,
      newId
    ) {
      konvaShape.id = newId;
    };

    /**
     * Function set Konva.shape draggable.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_setDraggable
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._setDraggable = function() {
      var konvaShapes = this._drawingLayer.children,
        i;

      for (i = 0; i < konvaShapes.length; i++) {
        if (konvaShapes[i].isMine) {
          konvaShapes[i].draggable(true);
        }
      }

      this._drawingLayer.on(
        'dragstart',
        function(e) {
          this._tool.dragStart(e, this._scale);
          var shape = e.target;
          var selfRect = shape.getSelfRect();

          var scaledSelfRect = {
            x: selfRect.x * this._scale,
            y: selfRect.y * this._scale,
            width: selfRect.width * this._scale,
            height: selfRect.height * this._scale
          };

          var previousPos = null;

          if (shape.dragBoundFunction) {
            shape.setDragBoundFunc(
              function(pos) {
                if (!previousPos) {
                  previousPos = pos;
                }
                var newPos = shape.dragBoundFunction(
                  shape,
                  pos,
                  scaledSelfRect,
                  this._canvas.getBoundingClientRect(),
                  previousPos,
                  this._scale
                );
                previousPos = newPos;
                return newPos;
              }.bind(this)
            );
          }
        }.bind(this)
      );

      this._drawingLayer.on(
        'dragend',
        function(e) {
          var shape = e.target;
          this._tool.dragEnd(e, this._scale);
          shape.setDragBoundFunc();
        }.bind(this)
      );
      this._drawingLayer.draw();
    };

    /**
     * Function unset Konva.shape draggable.
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_unsetDraggable
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @returns {void}
     */
    KonvaWhiteboardRenderer.prototype._unsetDraggable = function() {
      var konvaShapes = this._drawingLayer.children,
        i;
      for (i = 0; i < konvaShapes.length; i++) {
        if (konvaShapes[i].isMine) {
          konvaShapes[i].draggable(false);
        }
      }
      this._drawingLayer.off('dragstart');
      this._drawingLayer.off('dragend');
      this._drawingLayer.draw();
    };

    /**
     * Function clears mouse events from renderer
     *
     * @private
     * @function AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#_clearMouseEvents
     * @memberOf AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer#
     * @returns {void}
     */

    KonvaWhiteboardRenderer.prototype._clearMouseEvents = function() {
      this._canvas.onmouseup = undefined;
      this._canvas.onmouseleave = undefined;
    };

    AvayaClientServices.Renderer.Konva.KonvaWhiteboardRenderer = KonvaWhiteboardRenderer;
  })(AvayaClientServices);

  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */

  /**
   * @description
   * KonvaWhiteboardTools class provides the set of tools which might be used during the whiteboard sharing session.
   **/
  (function() {
    'use strict';

    var TEXT_TOOL_LINE_HEIGHT = 1.32;
    var KONVA_LINE_HEIGHT = 1.25;
    var TEXTAREA_MAX_LENGTH = 3500;

    // Percetange of a shape allowed to be dragged outside the canvas margins
    var DRAG_OUT_MARGIN = 0.9;

    // Number of points to keep inside of the canvas during a drag & drop move
    var REQUIRED_POINTS_IN_CANVAS = 5;

    /**
     * Describes the whiteboard selection tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaSelectionTool
     * @extends AvayaClientServices.Services.Collaboration.SelectionTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaSelectionTool() {}

    /**
     * Describes the whiteboard pen tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaPenTool
     * @extends AvayaClientServices.Services.Collaboration.PenTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaPenTool() {}

    /**
     * Describes the whiteboard marker tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaMarkerTool
     * @extends AvayaClientServices.Services.Collaboration.MarkerTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaMarkerTool() {}

    /**
     * Describes the whiteboard line tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaLineTool
     * @extends AvayaClientServices.Services.Collaboration.LineTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaLineTool() {}

    /**
     * Describes the whiteboard rectangle tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaRectangleTool
     * @extends AvayaClientServices.Services.Collaboration.RectangleTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaRectangleTool() {}

    /**
     * Describes the whiteboard circle tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaCircleTool
     * @extends AvayaClientServices.Services.Collaboration.CircleTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaCircleTool() {}

    /**
     * Describes the whiteboard stamp tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaStampTool
     * @extends AvayaClientServices.Services.Collaboration.StampTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaStampTool() {}

    /**
     * Describes the whiteboard text tool.
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaTextTool
     * @extends AvayaClientServices.Services.Collaboration.TextTool
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaTextTool() {}

    /**
     * @description
     * KonvaWhiteboardTools extends Collaboration module WhiteboardTools.
     * Provides set of tools which might be used during the whiteboard sharing session.
     *
     * @class
     * @define AvayaClientServices.Renderer.Konva.KonvaWhiteboardTools
     * @memberOf AvayaClientServices.Renderer.Konva
     */
    function KonvaWhiteboardTools(renderer) {
      AvayaClientServices.Services.Collaboration.WhiteboardTools.call(this);

      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaLineTool
       * */
      var lineTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.LINE
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaRectangleTool
       * */
      var rectangleTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.RECTANGLE
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaCircleTool
       * */
      var circleTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.CIRCLE
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaStampTool
       * */
      var stampTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.STAMP
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaSelectionTool
       * */
      var selectionTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.SELECTION
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaTextTool
       * */
      var textTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.TEXT
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaMarkerTool
       * */
      var markerTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.MARKER
      ];
      /**
       * @type AvayaClientServices.Renderer.Konva.KonvaPenTool
       * */
      var penTool = this[
        AvayaClientServices.Services.Collaboration.WhiteboardToolTypes.PEN
      ];

      /**
       * @description
       * This function is invoked during a drag and Drop operation of a Rect Shape to verify
       * if the new position of tha shape is inside the bounds specified for it.
       * For these shapes the x,y coordinates are the new position in the canvas of the
       * top left corner of the shape.
       *
       * @private
       * @function rectangleDragBoundFunction
       * @param {Konva.Shape} shape KonvaJS Shape being dragged - is only here to standardize the function signature
       * @param {object} pos {x, y} position of the shape
       * @param {object} scaledSelfRect {x, y, width, height} scale Self Rect of the shape
       * @param {Canvas} canvas Whiteboard canvas where the text should be rendered on.
       * @param {object} previousPos {x, y} last valid pos argument. - Only here to standardize the function signature
       * @param {number} scale Scale which currently applies to the whiteboard canvas. - Only here to standardize the function signature
       * @returns {object} JS Object with verified x,y coordinates for the new position
       */
      var rectangleDragBoundFunction = function(
        shape,
        pos,
        scaledSelfRect,
        canvas,
        previousPos,
        scale
      ) {
        // for Rect, pos argument has the position of the top left corner
        var minX = 0 - scaledSelfRect.width * DRAG_OUT_MARGIN;
        var maxX = canvas.width - scaledSelfRect.width * (1 - DRAG_OUT_MARGIN);
        var minY = 0 - scaledSelfRect.height * DRAG_OUT_MARGIN;
        var maxY =
          canvas.height - scaledSelfRect.height * (1 - DRAG_OUT_MARGIN);

        return {
          x: Math.min(maxX, Math.max(minX, pos.x)),
          y: Math.min(maxY, Math.max(minY, pos.y))
        };
      };

      /**
       * @description
       * This function is invoked during a drag and Drop of an Ellipse Shape to verify
       * if the new position of tha shape is inside the bounds specified for it
       *
       * @private
       * @function circleDragBoundFunction
       * @param {Konva.Shape} shape KonvaJS Shape being dragged - is only here to standardize the function signature
       * @param {object} pos {x, y} position of the shape
       * @param {object} scaledSelfRect {x, y, width, height} scale Self Rect of the shape
       * @param {Canvas} canvas Whiteboard canvas where the text should be rendered on.
       * @param {object} previousPos {x, y} last valid pos argument.
       * @param {number} scale Scale which currently applies to the whiteboard canvas. - Only here to standardize the function signature
       * @returns {object} JS Object with verified x,y coordinates for the new position
       */
      var circleDragBoundFunction = function(
        shape,
        pos,
        scaledSelfRect,
        canvas,
        previousPos,
        scale
      ) {
        // for Ellipse pos argument has the position of the center
        var outMarginWidth =
          scaledSelfRect.width -
          scaledSelfRect.width * DRAG_OUT_MARGIN -
          scaledSelfRect.width / 2;
        var outMarginHeight =
          scaledSelfRect.height -
          scaledSelfRect.height * DRAG_OUT_MARGIN -
          scaledSelfRect.height / 2;

        var minX = outMarginWidth;
        var maxX = canvas.width - outMarginWidth;
        var minY = outMarginHeight;
        var maxY = canvas.height - outMarginHeight;

        var newPos = {
          x: Math.min(maxX, Math.max(minX, pos.x)),
          y: Math.min(maxY, Math.max(minY, pos.y))
        };

        // Check if the shape is out of canvas margins in both axes
        if (
          (pos.x < 0 || pos.x > canvas.width) &&
          (pos.y < 0 || pos.y > canvas.height)
        ) {
          var corner = { x: 0, y: 0 };
          if (pos.x < 0) {
            corner.x = 0;
          } else {
            corner.x = canvas.width;
          }

          if (pos.y < 0) {
            corner.y = 0;
          } else {
            corner.y = canvas.height;
          }

          var minOutDistance = Math.min(outMarginWidth, outMarginHeight);
          var distanceFromCorner = Math.sqrt(
            Math.pow(pos.x - corner.x, 2) + Math.pow(pos.y - corner.y, 2)
          );

          if (distanceFromCorner > minOutDistance) {
            newPos = previousPos;
          }
        }

        return newPos;
      };

      var freeHandShapeIsOutOfAllowedMargins = function(
        shape,
        pos,
        canvas,
        scale
      ) {
        var pointsInCanvas = 0;
        var i = 0;
        var shapePoints = shape._modelRelativePoints;
        var requiredPointsInCanvas =
          shapePoints.length <= REQUIRED_POINTS_IN_CANVAS
            ? 2
            : REQUIRED_POINTS_IN_CANVAS;

        while (
          i < shapePoints.length &&
          pointsInCanvas < requiredPointsInCanvas
        ) {
          var movedPoint = {
            x: (shapePoints[i].x / shape._originalScale) * scale + pos.x,
            y: (shapePoints[i].y / shape._originalScale) * scale + pos.y
          };

          if (
            movedPoint.x > 0 &&
            movedPoint.x < canvas.width &&
            movedPoint.y > 0 &&
            movedPoint.y < canvas.height
          ) {
            pointsInCanvas++;
          }
          i++;
        }

        if (pointsInCanvas < requiredPointsInCanvas) {
          return true;
        } else {
          return false;
        }
      };

      var lineShapeIsOutOfAllowedMargins = function(shape, pos, canvas, scale) {
        var point1 = {
          x: shape.getPoints()[0] * scale + pos.x,
          y: shape.getPoints()[1] * scale + pos.y
        };
        var point2 = {
          x: shape.getPoints()[2] * scale + pos.x,
          y: shape.getPoints()[3] * scale + pos.y
        };

        var lineX = 0,
          lastX = 0;
        if (point1.x < point2.x) {
          lineX = point1.x;
          lastX = point2.x;
        } else {
          lineX = point1.x;
          lastX = point2.x;
        }

        var pointsInCanvas = 0;
        while (lineX < lastX && pointsInCanvas < REQUIRED_POINTS_IN_CANVAS) {
          // Equation of a line given two points
          var lineY =
            ((point2.y - point1.y) / (point2.x - point1.x)) *
              (lineX - point1.x) +
            point1.y;

          if (
            lineX > 0 &&
            lineX < canvas.width &&
            lineY > 0 &&
            lineY < canvas.height
          ) {
            pointsInCanvas++;
          }

          lineX += 4;
        }

        if (pointsInCanvas < REQUIRED_POINTS_IN_CANVAS) {
          return true;
        } else {
          return false;
        }
      };

      /**
       * @description
       * If the shape is moving out of the valid region, this function helps in identify a valid
       * position to return to the drag and drop move
       *
       * @private
       * @function findValidNewPosition
       * @param {Konva.Shape} shape KonvaJS Shape being dragged
       * @param {object} pos {x, y} position of the shape
       * @param {Canvas} canvas Whiteboard canvas where the text should be rendered on.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @param {object} previousPos {x, y} last valid pos argument. If the move is not valid this will be used
       *                             as a safe place to return
       * @param {object} newPos {x, y} default new position, for regular shapes
       * @returns {object} JS Object with verified x,y coordinates
       */
      var findValidNewPosition = function(
        shape,
        pos,
        canvas,
        scale,
        previousPos,
        newPos
      ) {
        var outOfMargins;
        if (shape._modelRelativePoints) {
          if (shape._shapeType && shape._shapeType === 'Stamp') {
            return newPos;
          }
          outOfMargins = freeHandShapeIsOutOfAllowedMargins;
        } else if (shape instanceof Konva.Line) {
          outOfMargins = lineShapeIsOutOfAllowedMargins;
        } else {
          return newPos;
        }

        if (outOfMargins(shape, pos, canvas, scale)) {
          // Try to rescue some delta of mouse movement
          var deltXPos = { x: pos.x, y: previousPos.y };
          if (!outOfMargins(shape, deltXPos, canvas, scale)) {
            return deltXPos;
          } else {
            var deltYPos = { x: previousPos.x, y: pos.y };
            if (!outOfMargins(shape, deltYPos, canvas, scale)) {
              return deltYPos;
            } else {
              return previousPos;
            }
          }
        }
        return newPos;
      };

      /**
       * @description
       * This function is invoked during a drag and Drop operation to verify
       * if the new position of tha shape is inside the bounds specified for it
       * This function works with a position which is relative to the initial position of the shape
       * This behaviour applies to: line, pen, marker, text and stamp shapes
       *
       * @private
       * @function relativePosDragBoundFunction
       * @param {Konva.Shape} shape KonvaJS Shape being dragged
       * @param {object} pos {x, y} position of the shape
       * @param {object} scaledSelfRect {x, y, width, height} scale Self Rect of the shape
       * @param {Canvas} canvas Whiteboard canvas where the text should be rendered on.
       * @param {object} previousPos {x, y} last valid pos argument. If the move is not valid this will be used
       *                             as a safe place to return
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {object} JS Object with verified x,y coordinates for the new position
       */
      var relativePosDragBoundFunction = function(
        shape,
        pos,
        scaledSelfRect,
        canvas,
        previousPos,
        scale
      ) {
        // Text has padding which add an extra white area
        var outMarginOnHeight =
          shape instanceof Konva.Text ? DRAG_OUT_MARGIN * 0.6 : DRAG_OUT_MARGIN;
        var outMarginOnWidth =
          shape instanceof Konva.Text
            ? DRAG_OUT_MARGIN * 0.85
            : DRAG_OUT_MARGIN;

        // for these Shapes the pos argument is an offset from the original position
        var minX = -scaledSelfRect.x - scaledSelfRect.width * outMarginOnWidth;
        var maxX =
          canvas.width -
          (scaledSelfRect.x + scaledSelfRect.width * (1 - outMarginOnWidth));
        var minY =
          -scaledSelfRect.y - scaledSelfRect.height * outMarginOnHeight;
        var maxY =
          canvas.height -
          (scaledSelfRect.y + scaledSelfRect.height * (1 - outMarginOnHeight));

        var newPos = {
          x: Math.min(maxX, Math.max(minX, pos.x)),
          y: Math.min(maxY, Math.max(minY, pos.y))
        };

        var outOfMarginOnXAxe =
          pos.x < -scaledSelfRect.x ||
          pos.x > canvas.width - scaledSelfRect.x - scaledSelfRect.width;
        var outOfMarginOnYAxe =
          pos.y < -scaledSelfRect.y ||
          pos.y > canvas.height - scaledSelfRect.y - scaledSelfRect.height;

        if (outOfMarginOnYAxe || outOfMarginOnXAxe) {
          newPos = findValidNewPosition(
            shape,
            pos,
            canvas,
            scale,
            previousPos,
            newPos
          );
        }
        return newPos;
      };

      /**
       * @description
       * Starts drawing the marker line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a marker tool is selected.
       *
       * @function AvayaClientServices.Renderer.Konva.KonvaMarkerTool#startFunction
       * @public
       * @param {object} event HTML DOM mousedown event
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Shape}
       */
      markerTool.startFunction = function(event, scale) {
        var konvaShape = new Konva.Shape({
          stroke: renderer._color,
          strokeWidth: 12,
          hitStrokeWidth: 20,
          lineCap: 'round',
          lineJoin: 'round',
          opacity: 0.6,
          sceneFunc: function(context) {
            context.beginPath();
            var i = 0;
            context.moveTo(konvaShape._modelStart.x, konvaShape._modelStart.y);
            for (i; i < konvaShape._modelRelativePoints.length; i++) {
              context.lineTo(
                konvaShape._modelRelativePoints[i].x / scale,
                konvaShape._modelRelativePoints[i].y / scale
              );
            }
            context.fillStrokeShape(this);
          }
        });
        konvaShape.setDragDistance(0);
        konvaShape.dragBoundFunction = relativePosDragBoundFunction;
        konvaShape._modelStart = {
          x: event.offsetX / scale,
          y: event.offsetY / scale
        };
        konvaShape._modelRelativePoints = [];
        konvaShape._originalScale = scale;
        konvaShape._selfRectCoordinates = {
          minX: 999999,
          minY: 999999,
          maxX: -999999,
          maxY: -999999
        };
        konvaShape.getSelfRect = function() {
          return {
            x: this._selfRectCoordinates.minX / scale,
            y: this._selfRectCoordinates.minY / scale,
            width:
              this._selfRectCoordinates.maxX / scale -
              this._selfRectCoordinates.minX / scale,
            height:
              this._selfRectCoordinates.maxY / scale -
              this._selfRectCoordinates.minY / scale
          };
        };
        renderer._drawingLayer.add(konvaShape);
        renderer._drawingLayer.draw();
        return konvaShape;
      }.bind(this);

      /**
       * @description
       * Draws the marker line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousemove event
       * on the whiteboard canvas when a marker tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaMarkerTool#drawFunction
       * @param {object} evt HTML DOM mousemove event.
       * @param {Konva.Shape} konvaShape Shape that is goint to be drawn.
       * @returns {void}
       */
      markerTool.drawFunction = function(evt, konvaShape) {
        konvaShape._modelRelativePoints.push({
          x: evt.offsetX,
          y: evt.offsetY
        });
        konvaShape._selfRectCoordinates.minX = Math.min(
          konvaShape._selfRectCoordinates.minX,
          evt.offsetX
        );
        konvaShape._selfRectCoordinates.minY = Math.min(
          konvaShape._selfRectCoordinates.minY,
          evt.offsetY
        );
        konvaShape._selfRectCoordinates.maxX = Math.max(
          konvaShape._selfRectCoordinates.maxX,
          evt.offsetX
        );
        konvaShape._selfRectCoordinates.maxY = Math.max(
          konvaShape._selfRectCoordinates.maxY,
          evt.offsetY
        );
        renderer._drawingLayer.draw();
        renderer._drawingStage.draw();
      };

      /**
       * @description
       * Finishes drawing the marker line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mouseup or mouseleave event
       * on the whiteboard canvas when a marker tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaMarkerTool#endFunction
       * @returns {void}
       */
      markerTool.endFunction = function() {
        renderer._canvas.onmousemove = undefined;
      }.bind(this);

      /**
       * @description
       * Starts drawing the pen line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a pen tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaPenTool#startFunction
       * @param {object} event HTML DOM mousedown event.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Shape}
       */
      penTool.startFunction = function(event, scale) {
        var konvaShape = new Konva.Shape({
          stroke: renderer._color,
          strokeWidth: 4,
          hitStrokeWidth: 20,
          lineCap: 'round',
          lineJoin: 'round',
          opacity: 1,
          sceneFunc: function(context) {
            context.beginPath();
            var i = 0;
            context.moveTo(konvaShape._modelStart.x, konvaShape._modelStart.y);
            for (i; i < konvaShape._modelRelativePoints.length; i++) {
              context.lineTo(
                konvaShape._modelRelativePoints[i].x / scale,
                konvaShape._modelRelativePoints[i].y / scale
              );
            }
            context.fillStrokeShape(this);
          }
        });

        konvaShape.dragBoundFunction = relativePosDragBoundFunction;
        konvaShape._modelStart = {
          x: event.offsetX / scale,
          y: event.offsetY / scale
        };
        konvaShape._modelRelativePoints = [];
        konvaShape._originalScale = scale;
        konvaShape._selfRectCoordinates = {
          minX: 999999,
          minY: 999999,
          maxX: -999999,
          maxY: -999999
        };
        konvaShape.getSelfRect = function() {
          return {
            x: this._selfRectCoordinates.minX / scale,
            y: this._selfRectCoordinates.minY / scale,
            width:
              this._selfRectCoordinates.maxX / scale -
              this._selfRectCoordinates.minX / scale,
            height:
              this._selfRectCoordinates.maxY / scale -
              this._selfRectCoordinates.minY / scale
          };
        };
        konvaShape.setDragDistance(0);
        renderer._drawingLayer.add(konvaShape);
        renderer._drawingLayer.draw();
        return konvaShape;
      }.bind(this);

      /**
       * @description
       * Draws the pen line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousemove event
       * on the whiteboard canvas when a marker tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaPenTool#drawFunction
       * @param {object} evt HTML DOM mousemove event.
       * @param {Konva.Shape} konvaShape Shape that is goint to be drawn.
       * @returns {void}
       */
      penTool.drawFunction = function(evt, konvaShape) {
        konvaShape._modelRelativePoints.push({
          x: evt.offsetX,
          y: evt.offsetY
        });
        konvaShape._selfRectCoordinates.minX = Math.min(
          konvaShape._selfRectCoordinates.minX,
          evt.offsetX
        );
        konvaShape._selfRectCoordinates.minY = Math.min(
          konvaShape._selfRectCoordinates.minY,
          evt.offsetY
        );
        konvaShape._selfRectCoordinates.maxX = Math.max(
          konvaShape._selfRectCoordinates.maxX,
          evt.offsetX
        );
        konvaShape._selfRectCoordinates.maxY = Math.max(
          konvaShape._selfRectCoordinates.maxY,
          evt.offsetY
        );
        renderer._drawingLayer.draw();
        renderer._drawingStage.draw();
      };

      /**
       * @description
       * Finishes drawing line with pen tool on the whiteboard canvas.
       * Function is being invoked when the user triggers mouseup or mouseleave event
       * on the whiteboard canvas when a pen tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaPenTool#endFunction
       * @returns {void}
       */
      penTool.endFunction = function() {
        renderer._canvas.onmousemove = undefined;
      }.bind(this);

      /**
       * Returns ID to operate on corresponding HTML element.
       *
       * @private
       * @function AvayaClientServices.Renderer.Konva.KonvaTextTool#_getElementId
       * @returns {string}
       */
      textTool._getElementId = function() {
        return 'canvasTextInput';
      };

      /**
       * @description
       * Removes an input text element and creates konvaText which is going to be rendered.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a text tool is selected and the user
       * is typing text.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaTextTool#endInput
       * @param {object} event HTML DOM mousedown event.
       * @param {Canvas} canvas Whiteboard canvas where the text should be rendered on.
       * @param {string} textInput Text that is going to be rendered.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Text}
       */
      textTool.endInput = function(event, canvas, textInput, scale) {
        var fontSize = parseInt(textInput.style.fontSize.replace('px', ''));

        var additionalX = 0;
        if (getComputedStyle) {
          var borderWidth = parseInt(
            getComputedStyle(textInput).borderBottomWidth
          );
          var paddingLeft = parseInt(getComputedStyle(textInput).paddingLeft);

          if (!!borderWidth) {
            additionalX += borderWidth;
          }

          if (!!paddingLeft) {
            additionalX += paddingLeft;
          }
        }

        var konvaText = new Konva.Text({
          x: parseInt(textInput.style.left) / scale + additionalX,
          y: parseInt(textInput.style.top) / scale,
          text: textInput.value,
          fontFamily: 'Arial',
          lineHeight: KONVA_LINE_HEIGHT,
          fontSize: fontSize
        });
        konvaText.isMine = true;
        konvaText.fontStyle('bold');
        konvaText.dragBoundFunction = relativePosDragBoundFunction;

        renderer._inputExists = false;
        textInput.parentNode.removeChild(textInput);
        return konvaText;
      };

      /**
         * @description
         * Creates an input text element where the user can type text.
         * Function is being invoked when the user triggers mousedown event
         * on the whiteboard canvas when a text tool is selected.

         * @public
         * @function AvayaClientServices.Renderer.Konva.KonvaTextTool#startInput
         * @param {object} event HTML DOM mousedown event.
         * @param {Canvas} canvas Whiteboard canvas where the text should be rendered on.
         * @param {string} mode Tool mode: filled or stroke.
         * @param {number} scale Scale which currently applies to the whiteboard canvas.
         * @returns {void}
         */
      textTool.startInput = function(event, canvas, mode, scale) {
        var canvasWidth = canvas.getBoundingClientRect().width;
        var canvasHeight = canvas.getBoundingClientRect().height;
        var textarea = document.createElement('textarea');
        var theWidth = 20;
        var rows = 1;
        var textareaInitTop = event.offsetY;
        var textareaInitLeft = event.offsetX;
        var fontSizeScaled =
          AvayaClientServices.Services.Collaboration.WhiteboardTextSizes[mode] *
          scale;
        var wasPasted = false;

        textarea.setAttribute('id', this._getElementId());
        textarea.setAttribute('rows', rows);
        textarea.style.cssText =
          "z-Index: 101; outline: 0; font-family: 'Arial'; font-weight:bold; font-size:" +
          fontSizeScaled +
          'px; position: absolute; top:' +
          event.offsetY.toString() +
          'px; left:' +
          event.offsetX.toString() +
          'px; overflow: hidden; width:' +
          theWidth +
          'px; line-height:' +
          TEXT_TOOL_LINE_HEIGHT +
          '; height: auto; margin:0; border: 1px solid rgb(106, 105, 105); background: white; border-radius: 1px; margin-top: -2px; white-space: pre; color:' +
          renderer._color;
        canvas.parentNode.appendChild(textarea);

        setTimeout(function() {
          textarea.focus();
        });

        function updateDynamicVariables() {
          fontSizeScaled = parseInt(textarea.style.fontSize.replace('px', ''));
          canvasWidth = canvas.getBoundingClientRect().width;
          canvasHeight = canvas.getBoundingClientRect().height;
        }

        function updateTextAreaRows(str) {
          updateDynamicVariables();
          rows = 1;
          str.split('').forEach(function(el, ind, arr) {
            if (el === '\n') {
              rows = rows + 1;
              return;
            }
          });
          textarea.setAttribute('rows', rows);

          // for IE textarea
          var browser = AvayaClientServices.Base.Utils.getBrowserType();
          if (browser === AvayaClientServices.Base.BrowserType.IE) {
            var textareaId = textarea.id;
            var elem = $('#' + textareaId);
            if (elem.length) {
              elem.height(TEXT_TOOL_LINE_HEIGHT * rows * fontSizeScaled);
            }
          }
        }

        function arrayConverter(arrayOfStrings) {
          updateDynamicVariables();
          var arrayOfValContentString = arrayOfStrings.join('\n').split('');
          // string start index
          var startingIndex = 0;
          var maxRows = Math.round(
            canvasHeight / (fontSizeScaled * TEXT_TOOL_LINE_HEIGHT)
          );
          // max amount of symbols in row
          var MAX_CHARS_IN_ROW = 151;

          for (var index = 0; index < arrayOfStrings.length; index++) {
            var str = arrayOfStrings[index];

            var textareaWidth = textTool._calculateWidth(str, fontSizeScaled);
            if (textareaWidth > canvasWidth) {
              // number of substrings with a width canvas in string
              var numberOfSubstrings = Math.ceil(textareaWidth / canvasWidth);
              // substring start index
              var indexElemSubstr = 0;

              for (var i = 0; i < numberOfSubstrings; i++) {
                var substr = str.slice(
                  indexElemSubstr,
                  indexElemSubstr + MAX_CHARS_IN_ROW
                );

                for (
                  var iteration = 0;
                  iteration < TEXTAREA_MAX_LENGTH &&
                  textTool._calculateWidth(substr, fontSizeScaled) >
                    canvasWidth;
                  iteration++
                ) {
                  substr = substr.substring(0, substr.length - 1);
                }

                var arrayValSubstr = substr.split('');
                var indexOfLastSpace = substr.indexOf(' ');

                if (
                  indexOfLastSpace > -1 &&
                  indexElemSubstr + substr.length !== str.length
                ) {
                  arrayOfValContentString.splice(
                    arrayValSubstr.lastIndexOf(' ') +
                      startingIndex +
                      indexElemSubstr,
                    1,
                    '\n'
                  );
                  indexElemSubstr =
                    indexElemSubstr + arrayValSubstr.lastIndexOf(' ') + 1;
                } else if (indexElemSubstr + substr.length !== str.length) {
                  arrayOfValContentString.splice(
                    arrayValSubstr.length + startingIndex + indexElemSubstr,
                    0,
                    '\n'
                  );
                  indexElemSubstr = arrayValSubstr.length + indexElemSubstr;
                }
              }
            }
            startingIndex += str.length + 1;
          }

          var contentStr = arrayOfValContentString.join('');
          var arr = contentStr.split('\n');
          if (arr.length > maxRows) {
            contentStr = arr.slice(0, maxRows).join('\n');
          }
          return contentStr;
        }

        function keydownHandler(e) {
          updateDynamicVariables();
          var contentString = e.target.value;

          if (contentString.length > TEXTAREA_MAX_LENGTH) {
            e.target.value = contentString.slice(0, TEXTAREA_MAX_LENGTH);
            contentString = e.target.value;
          }
          var contentLength = contentString.length;
          var lastSymbol = e.target.value.charCodeAt(contentLength - 1);

          if (e.inputType === 'insertFromPaste' || wasPasted) {
            var arrayOfStrings = e.target.value.split('\n');
            var maxRows = Math.round(
              canvasHeight / (fontSizeScaled * TEXT_TOOL_LINE_HEIGHT)
            );
            if (arrayOfStrings.length > maxRows) {
              arrayOfStrings = arrayOfStrings.slice(0, maxRows);
            }
            contentString = arrayConverter(arrayOfStrings);
            e.target.value = contentString;
          }

          if (
            lastSymbol !== 10 ||
            e.inputType === 'insertFromPaste' ||
            wasPasted
          ) {
            theWidth = textTool._calculateWidth(contentString, fontSizeScaled);

            if (theWidth < canvasWidth) {
              //if textarea IS NOT WIDER than canvas
              textarea.style.width = theWidth + 'px';

              if (
                textarea.getBoundingClientRect().left + theWidth >
                canvas.getBoundingClientRect().right
              ) {
                //if textarea field overflows canvas
                var delta =
                  textarea.getBoundingClientRect().left +
                  theWidth -
                  canvas.getBoundingClientRect().right;
                textareaInitLeft = textareaInitLeft - delta;
                textarea.style.left = textareaInitLeft + 'px';
              }
              updateTextAreaRows(e.target.value);
            } else {
              textarea.style.left = 0;
              textarea.style.width = canvasWidth + 'px';

              var lastString = e.target.value.split('\n').slice(-1)[0];
              var lastStringWidth = textTool._calculateWidth(
                lastString,
                fontSizeScaled
              );
              var indexOfLastSpace = lastString.indexOf(' ');
              var strValArr = contentString.split('');

              if (lastStringWidth > canvasWidth) {
                if (indexOfLastSpace > -1) {
                  //if last string contains space
                  strValArr.splice(strValArr.lastIndexOf(' '), 1, '\n');
                  e.target.value = strValArr.join('');
                  updateTextAreaRows(e.target.value);
                } else {
                  //if last string doesn't contain space
                  strValArr.splice(-1, 0, '\n');
                  e.target.value = strValArr.join('');

                  updateTextAreaRows(e.target.value);
                }
              }
            }
          } else if (contentLength === 1) {
            e.target.value = '';
          } else {
            updateTextAreaRows(e.target.value);
          }
          wasPasted = false;
          textTool._checkOutOfBounds(textarea, canvas);
        }

        function keypressHandler(e) {
          updateDynamicVariables();
          var contentString = e.target.value;
          var contentLength = contentString.length;

          var keyCode = e.keyCode;
          var symbol = String.fromCharCode(e.keyCode);

          var textareadHeight = textarea.clientHeight;

          var canvasWidth = canvas.clientWidth;
          var canvasHeight = canvas.clientHeight;

          // Is it last row? If not - just new row will added if there is no space in current row
          if (textareadHeight + fontSizeScaled >= canvasHeight) {
            // If press enter - first row goes out of canvas, but it's last row - cancel input
            if (keyCode === 13) {
              e.preventDefault();
              return false;

              // Check if there is last char on last row - first row also goes out of canvas
            } else {
              var insertIndex = e.target.selectionStart;
              var substringStart = 0;
              var substringEnd;

              // Search where string ends
              var newStringArr = contentString.split('');

              // Which string we receive after input
              newStringArr.splice(insertIndex, 0, symbol);
              var newString = newStringArr.join('');

              for (var i = insertIndex; i < contentLength + 1; i++) {
                if (newString[i] === '\n') {
                  substringEnd = i;
                  break;
                }
              }

              // Search where string starts
              for (var j = insertIndex; j >= 0; j--) {
                if (newString[j] === '\n') {
                  substringStart = j;
                  break;
                }
              }

              if (substringEnd) {
                substringEnd += 1;
              }

              var currentString = newString.slice(
                substringStart + 1,
                substringEnd
              );
              var currentStringWidth = textTool._calculateWidth(
                currentString,
                fontSizeScaled
              );

              // Last row and if current string greater than canvasWidth - it will lead to new row and first will go out - so, cancel input
              if (currentStringWidth > canvasWidth) {
                e.preventDefault();
                return false;
              }
            }
          }

          textTool._checkOutOfBounds(textarea, canvas);
        }

        textTool._checkOutOfBounds(textarea, canvas);
        textarea.addEventListener('keypress', keypressHandler);
        textarea.addEventListener('input', keydownHandler);
        textarea.addEventListener('paste', function() {
          wasPasted = true;
        });

        return {
          initialTop: textareaInitTop,
          initialLeft: textareaInitLeft,
          initialCanvasHeigth: canvasHeight,
          initialCanvasWidth: canvasWidth,
          textTool: this
        };
      };

      textTool._calculateWidth = function(str, fontSize) {
        var auxillaryEl = document.createElement('span');
        auxillaryEl.style.cssText =
          'font-weight:bold; top:400px; left:300px; line-height: normal; visibility:hidden; borderRadius: 1px; position: absolute; border: 1px  solid rgb(106, 105, 105); margin: 0; white-space: pre; padding:2px; display: inline-block; font-size:' +
          fontSize +
          "px; font-family:'Arial'";
        auxillaryEl.innerHTML = str;
        document.body.appendChild(auxillaryEl);
        var textWidth = auxillaryEl.getBoundingClientRect().width;
        setTimeout(function() {
          document.body.removeChild(auxillaryEl);
        }, 100);
        return textWidth;
      };

      textTool._adjustWidth = function(textarea) {
        var text = textarea.value ? textarea.value : ' ';
        var fontSize = parseInt(textarea.style.fontSize.replace('px', ''));

        var updatedWidth = textTool._calculateWidth(text, fontSize);
        textarea.style.width = updatedWidth + 'px';
      };

      textTool._checkOutOfBounds = function(textarea, canvas) {
        var textareadHeight = textarea.clientHeight;
        var textareadWidth = textarea.clientWidth;

        var textareaTop = textarea.offsetTop;
        var textareaLeft = textarea.offsetLeft;

        // Out of bottom
        if (textareadHeight + textareaTop > canvas.clientHeight) {
          var newTopPosition = canvas.clientHeight - textareadHeight;
          if (newTopPosition < 0) {
            newTopPosition = 0;
          }
          textarea.style.top = newTopPosition + 'px';
        }

        // Out of right
        if (textareadWidth + textareaLeft > canvas.clientWidth) {
          var newLeftPosition = canvas.clientWidth - textareadWidth;
          if (newLeftPosition < 0) {
            newLeftPosition = 0;
          }
          textarea.style.left = newLeftPosition + 'px';
        }
      };

      textTool._zoom = function(textarea, textareaData, canvas, mode, scale) {
        var canvasWidth = canvas.getBoundingClientRect().width;
        var canvasHeight = canvas.getBoundingClientRect().height;

        var newTopPosition =
          (canvasHeight * textareaData.initialTop) /
          textareaData.initialCanvasHeigth;
        textarea.style.top = newTopPosition + 'px';

        var newLetPosition =
          (canvasWidth * textareaData.initialLeft) /
          textareaData.initialCanvasWidth;
        textarea.style.left = newLetPosition + 'px';

        var fontSizeScaled =
          AvayaClientServices.Services.Collaboration.WhiteboardTextSizes[mode] *
          scale;
        textarea.style.fontSize = fontSizeScaled + 'px';

        this._adjustWidth(textarea);
        this._checkOutOfBounds(textarea, canvas);
      };

      /**
       * @description
       * Starts process of moving the shape within the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a selection tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaSelectionTool#dragStart
       * @param {object} event HTML DOM mousedown event.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {void}
       */
      selectionTool.dragStart = function(event, scale) {
        var shape = event.target;
        this._clickData = {
          eventX: event.evt.pageX / scale,
          eventY: event.evt.pageY / scale,
          x: shape.x(),
          y: shape.y()
        };
      };

      /**
       * @description
       * Finishes process of moving the shape within the whiteboard canvas.
       * Function is being invoked when the user triggers mouseup event
       * on the whiteboard canvas when a selection tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaSelectionTool#dragEnd
       * @param {object} event HTML DOM mouseup event.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {void}
       */
      selectionTool.dragEnd = function(event, scale) {
        if (!this._clickData) {
          return;
        }

        var shape = event.target;

        var dfd = renderer._moveShape(event.target, {
          selectionX: this._clickData.x,
          selectionY: this._clickData.y,
          newPositionX: shape.x(),
          newPositionY: shape.y()
        });
        this._clickData = undefined;
        //            return dfd;
      };

      /**
       * @description
       * Starts drawing the line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a line tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaLineTool#startFunction
       * @param {object} event HTML DOM mousedown event.
       * @param {string} mode Tool mode: filled or stroke.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Line}
       */
      lineTool.startFunction = function(event, mode, scale) {
        var start = { x: event.offsetX / scale, y: event.offsetY / scale };
        var end = { x: event.offsetX / scale, y: event.offsetY / scale };
        //EDGEONLY JSCSDK-4181
        if (start.x === end.x && start.y === end.y) {
          end.x += 0.1;
          end.y += 0.1;
        }
        var line = new Konva.Line({
          points: [start.x, start.y, end.x, end.y],
          stroke: renderer._color,
          strokeWidth:
            AvayaClientServices.Services.Collaboration.WhiteboardLineWidths[
              mode
            ],
          hitStrokeWidth: 20,
          lineCap: 'round',
          lineJoin: 'round'
        });

        line.setDragDistance(0);
        line.dragBoundFunction = relativePosDragBoundFunction;
        renderer._drawingLayer.add(line);
        renderer._drawingLayer.draw();
        renderer._canvas.onmousemove = function(evt) {
          if (evt.shiftKey) {
            var deltaX = evt.offsetX / scale - start.x;
            var deltaY = evt.offsetY / scale - start.y;

            // All four parts of the trigonometric circle works the same (due to abs and one horizontal line)
            var radians = Math.atan(deltaY / deltaX);

            // More than a bisector between 45 degrees and 90 degrees - up
            if (Math.abs(radians) > (3 * Math.PI) / 2 / 2 / 2) {
              end.x = start.x;
              end.y = evt.offsetY / scale;

              // More than a bisector between 0 degrees and 45 degrees - diagonal
            } else if (Math.abs(radians) > Math.PI / 2 / 2 / 2) {
              end.x = evt.offsetX / scale;
              end.y = start.y + (end.x - start.x) * Math.sign(radians);

              // Otherwise - lower, horizontal line
            } else {
              end.x = evt.offsetX / scale;
              end.y = start.y;
            }
          } else {
            end.x = evt.offsetX / scale;
            end.y = evt.offsetY / scale;
          }

          line.attrs.points = [start.x, start.y, end.x, end.y];
          renderer._drawingLayer.draw();
          renderer._drawingStage.draw();
        }.bind(this);
        return line;
      }.bind(this);

      /**
       * @description
       * Finishes drawing the line shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mouseup event
       * on the whiteboard canvas when a line tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaLineTool#endFunction
       * @param {object} event HTML DOM mouseup event.
       * @returns {void}
       */
      lineTool.endFunction = function(event) {
        renderer._canvas.onmousemove = undefined;
      }.bind(this);

      /**
       * @description
       * Starts drawing the rectangle shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a rectangle tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaRectangleTool#startFunction
       * @param {object} event HTML DOM mousedown event.
       * @param {string} mode Tool mode: filled or stroke.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Rect}
       */
      rectangleTool.startFunction = function(event, mode, scale) {
        var rect;
        if (mode === 'FILLED') {
          rect = new Konva.Rect({
            x: event.offsetX / scale,
            y: event.offsetY / scale,
            width: 1,
            height: 1,
            fill: renderer._color
          });
        } else if (mode === 'STROKE') {
          rect = new Konva.Rect({
            x: event.offsetX / scale,
            y: event.offsetY / scale,
            width:
              AvayaClientServices.Services.Collaboration.WhiteboardLineWidths
                .LARGE,
            height: 1,
            lineCap: 'round',
            lineJoin: 'round',
            stroke: renderer._color,
            strokeWidth: renderer._strokeWidth
          });
        }
        rect.setDragDistance(0);

        // Set a helper function to the Konva Shape to control de drag and drop boundings
        rect.dragBoundFunction = rectangleDragBoundFunction;

        renderer._drawingLayer.add(rect);
        renderer._drawingLayer.draw();
        renderer._canvas.onmousemove = function(evt) {
          if (evt.shiftKey) {
            var size = Math.min(
              evt.offsetY / scale - rect.attrs.y,
              evt.offsetX / scale - rect.attrs.x
            );
            if (evt.offsetY < rect.attrs.y && evt.offsetX > rect.attrs.x) {
              rect.width(-size);
              rect.height(size);
            } else if (
              evt.offsetY > rect.attrs.y &&
              evt.offsetX < rect.attrs.x
            ) {
              rect.width(size);
              rect.height(-size);
            } else {
              rect.width(size);
              rect.height(size);
            }
          } else {
            rect.width(evt.offsetX / scale - rect.attrs.x);
            rect.height(evt.offsetY / scale - rect.attrs.y);
          }

          renderer._drawingLayer.draw();
          renderer._drawingStage.draw();
        }.bind(this);
        return rect;
      }.bind(this);

      /**
       * @description
       * Finishes drawing the rectangle shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mouseup event
       * on the whiteboard canvas when a rectangle tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaRectangleTool#endFunction
       * @param {object} event HTML DOM mouseup event.
       * @returns {void}
       */
      rectangleTool.endFunction = function(event) {
        renderer._canvas.onmousemove = undefined;
      }.bind(this);

      /**
       * @description
       * Starts drawing the circle shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a circle tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaCircleTool#startFunction
       * @param {object} event HTML DOM mousedown event.
       * @param {string} mode Tool mode: filled or stroke.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Ellipse}
       */
      circleTool.startFunction = function(event, mode, scale) {
        var topLeft = { x: event.offsetX / scale, y: event.offsetY / scale };
        var bottomRight = {
          x: event.offsetX / scale,
          y: event.offsetY / scale
        };
        var circle;
        if (mode === 'FILLED') {
          circle = new Konva.Ellipse({
            x: topLeft.x + (bottomRight.x - topLeft.x) / 2,
            y: topLeft.y + (bottomRight.y - topLeft.y) / 2,
            radius: {
              x: Math.abs(bottomRight.x - topLeft.x) / 2,
              y: Math.abs(bottomRight.y - topLeft.y) / 2
            },
            fill: renderer._color
          });
        } else if (mode === 'STROKE') {
          circle = new Konva.Ellipse({
            x: topLeft.x + (bottomRight.x - topLeft.x) / 2,
            y: topLeft.y + (bottomRight.y - topLeft.y) / 2,
            radius: {
              x: Math.abs(bottomRight.x - topLeft.x) / 2,
              y: Math.abs(bottomRight.y - topLeft.y) / 2
            },
            stroke: renderer._color,
            strokeWidth: renderer._strokeWidth
          });
        }
        circle.setDragDistance(0);
        circle.dragBoundFunction = circleDragBoundFunction;
        renderer._drawingLayer.add(circle);
        renderer._drawingLayer.draw();
        renderer._canvas.onmousemove = function(event) {
          bottomRight.x = event.offsetX / scale;
          bottomRight.y = event.offsetY / scale;

          if (event.shiftKey) {
            var diameter = Math.min(
              bottomRight.y - topLeft.y,
              bottomRight.x - topLeft.x
            );

            circle.radius({
              x: Math.abs(diameter) / 2,
              y: Math.abs(diameter) / 2
            });
            if (bottomRight.x < topLeft.x && bottomRight.y > topLeft.y) {
              circle.x(topLeft.x + diameter / 2);
              circle.y(topLeft.y - diameter / 2);
              circle.initX = topLeft.x + diameter / 2;
              circle.initY = topLeft.y - diameter / 2;
            } else if (bottomRight.x > topLeft.x && bottomRight.y < topLeft.y) {
              circle.x(topLeft.x - diameter / 2);
              circle.y(topLeft.y + diameter / 2);
              circle.initX = topLeft.x - diameter / 2;
              circle.initY = topLeft.y + diameter / 2;
            } else {
              circle.x(topLeft.x + diameter / 2);
              circle.y(topLeft.y + diameter / 2);
              circle.initX = topLeft.x + diameter / 2;
              circle.initY = topLeft.y + diameter / 2;
            }
          } else {
            circle.radius({
              x: Math.abs(bottomRight.x - topLeft.x) / 2,
              y: Math.abs(bottomRight.y - topLeft.y) / 2
            });
            circle.x(topLeft.x + (bottomRight.x - topLeft.x) / 2);
            circle.y(topLeft.y + (bottomRight.y - topLeft.y) / 2);
            circle.initX = topLeft.x + (bottomRight.x - topLeft.x) / 2;
            circle.initY = topLeft.y + (bottomRight.y - topLeft.y) / 2;
          }

          renderer._drawingLayer.draw();
          renderer._drawingStage.draw();
        }.bind(this);
        return circle;
      }.bind(this);

      /**
       * @description
       * Finishes drawing the circle shape.
       * Function is being invoked when the user triggers mouseup event
       * on the whiteboard canvas when a circle tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaCircleTool#endFunction
       * @param {object} event HTML DOM mouseup event.
       * @returns {void}
       */
      circleTool.endFunction = function(event) {
        renderer._canvas.onmousemove = undefined;
      }.bind(this);

      /**
       * @description
       * Draws the stamp shape on the whiteboard canvas.
       * Function is being invoked when the user triggers mousedown event
       * on the whiteboard canvas when a stamp tool is selected.
       *
       * @public
       * @function AvayaClientServices.Renderer.Konva.KonvaStampTool#drawFunction
       * @param {object} event HTML DOM mousedown event.
       * @param {number} scale Scale which currently applies to the whiteboard canvas.
       * @returns {Konva.Shape}
       */
      stampTool.drawFunction = function(event, scale) {
        var start = { x: event.offsetX / scale, y: event.offsetY / scale };
        var points = [
          { x: 0, y: 0 },
          { x: -10, y: -10 },
          { x: -42, y: -10 },
          { x: -42, y: 10 },
          { x: -10, y: 10 }
        ];
        var stamp = new Konva.Shape({
          fill: renderer._color,
          sceneFunc: function(context) {
            context.beginPath();
            context.moveTo(start.x, start.y);
            var i = 1;
            for (i; i < points.length; i++) {
              context.lineTo(start.x + points[i].x, start.y + points[i].y);
            }
            context.closePath();
            context.fillStrokeShape(this);
          }
        });
        stamp._modelStart = start;
        stamp._modelRelativePoints = points;
        stamp._originalScale = scale;
        stamp._shapeType = 'Stamp';

        stamp._selfRectCoordinates = {
          minX: start.x - 42,
          minY: start.y - 10,
          maxX: start.x,
          maxY: start.y + 10
        };
        stamp.getSelfRect = function() {
          return {
            x: this._selfRectCoordinates.minX,
            y: this._selfRectCoordinates.minY,
            width:
              this._selfRectCoordinates.maxX - this._selfRectCoordinates.minX,
            height:
              this._selfRectCoordinates.maxY - this._selfRectCoordinates.minY
          };
        };
        stamp.setDragDistance(0);
        stamp.dragBoundFunction = relativePosDragBoundFunction;

        renderer._drawingLayer.add(stamp);
        renderer._drawingLayer.draw();
        renderer._drawingStage.draw();
        return stamp;
      }.bind(this);
    }

    KonvaWhiteboardTools.prototype = Object.create(
      AvayaClientServices.Services.Collaboration.WhiteboardTools.prototype
    );

    AvayaClientServices.Renderer.Konva.KonvaWhiteboardTools = KonvaWhiteboardTools;
  })();
  /*
   * Avaya Inc. Proprietary (Restricted)
   * Solely for authorized persons having a need to know
   * pursuant to company instructions.
   * Copyright 2006-2019 Avaya Inc. All Rights Reserved.
   * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
   * The copyright notice above does not evidence any actual or
   * intended publication of such source code.
   */
  (function(AvayaClientServices) {
    'use strict';

    /**
     * @readonly
     * @enum {String}
     * @memberOf AvayaClientServices.Renderer
     * @define AvayaClientServices.Renderer.LoggerTags
     */
    var LoggerTags = {
      KONVA_WHITEBOARD_RENDERER: 'KonvaWhiteboardRenderer',
      KONVA_CONTENT_SHARING_RENDERER: 'KonvaContentSharingRenderer'
    };

    AvayaClientServices.Renderer.LoggerTags = LoggerTags;
  })(AvayaClientServices);
  AvayaClientServices.Base.Library.Service.jQuery.addOnVersionChangedCallback(
    function(newVersion) {
      $ = newVersion;
    }
  );
})(jQuery);
